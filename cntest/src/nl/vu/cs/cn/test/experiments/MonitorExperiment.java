package nl.vu.cs.cn.test.experiments;

import org.junit.Test;

public class MonitorExperiment {
  @Test
  public void monitorExperiment() throws InterruptedException {
    final Object monitor = new Object();
    final Object threadMonitor = new Object();
    
    Thread t = new Thread() {
      public void run() {
        while(true) {
          synchronized(threadMonitor) {
            try {
              System.out.println("Thread waiting...");
              threadMonitor.wait();
              
              System.out.println("Thread doing 'work'...");
              Thread.sleep(50);
              
              System.out.println("Thread notifies process.");
              synchronized(monitor) { monitor.notify(); }
            }
            catch(InterruptedException e) {}
          }
        }
      }
    };
  
    t.start();
    
    // wait a little while for the thread to have started waiting
    Thread.sleep(75);
    
    for(int i = 0; i < 2; i++) {
      synchronized(monitor) {
        System.out.println("Process notifies thread.");
        synchronized(threadMonitor) { threadMonitor.notify(); }
        
        System.out.println("Process waiting...");
        monitor.wait();
        
        System.out.println("Process doing 'work'...");
        Thread.sleep(50);
      }
      
    }
    
    t.join(200);
  }
}
