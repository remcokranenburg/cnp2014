package nl.vu.cs.cn.test.unit;

import static org.junit.Assert.*;
import nl.vu.cs.cn.tcp.ConnectionState;

import org.junit.Before;
import org.junit.Test;

public class TestConnectionState {
  volatile boolean pastBlock;
  
  @Before
  public void reset() {
    pastBlock = false;
  }
  
  @Test
  public void testGet() {
    ConnectionState state = new ConnectionState(ConnectionState.S.LISTEN);
    assertEquals(ConnectionState.S.LISTEN, state.get());
  }
  
  @Test
  public void testSet() {
    ConnectionState state = new ConnectionState(ConnectionState.S.LISTEN);
    state.set(ConnectionState.S.ESTABLISHED);
    assertEquals(ConnectionState.S.ESTABLISHED, state.get());
  }
  
  @Test
  public void testBlockUntil() {
    try {
      final ConnectionState state = new ConnectionState(ConnectionState.S.LISTEN);
      
      Thread blockThread = new Thread() {
        public void run() {
          state.blockUntil(ConnectionState.S.CLOSED);
          pastBlock = true;
        }
      };
      
      blockThread.start();
      Thread.sleep(200);
      
      assertEquals(ConnectionState.S.LISTEN, state.get());
      assertFalse(pastBlock);
      
      state.set(ConnectionState.S.ESTABLISHED);
      Thread.sleep(100);
      
      assertFalse(pastBlock);
      
      state.set(ConnectionState.S.CLOSED);
      Thread.sleep(100);
      
      assertTrue(pastBlock);
    
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
}
