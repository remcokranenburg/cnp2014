package nl.vu.cs.cn.test.unit;

import static org.junit.Assert.*;
import nl.vu.cs.cn.IP.IpAddress;

import org.junit.Test;

public class TestIpAddress {
  @Test
  public void testGetAddressInt() {
    IpAddress addr = IpAddress.getAddress(192 | 168 << 8 | 1 << 24);
    assertEquals("192.168.0.1", addr.toString());
  }
  
  @Test
  public void testGetAddressStringValid() {
    IpAddress addr = IpAddress.getAddress("192.168.0.1");
    assertEquals(192 | 168 << 8 | 1 << 24, addr.getAddress());
  }
  
  @Test(expected=IllegalArgumentException.class)
  public void testGetAddressStringInvalid() {
    IpAddress.getAddress("invalid");
  }
  
  @Test(expected=NumberFormatException.class)
  public void testGetAddressStringJustDots() {
    IpAddress.getAddress("asd.asd.asd.asd");
  }
}
