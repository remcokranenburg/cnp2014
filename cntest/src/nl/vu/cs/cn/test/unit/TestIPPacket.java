package nl.vu.cs.cn.test.unit;

import static org.junit.Assert.*;
import nl.vu.cs.cn.IP;

import org.junit.Test;

public class TestIPPacket {
  
  @Test
  public void testToStringEmpty() {
    IP.Packet packet = new IP.Packet(192 | 168 << 8 | 1 << 24, IP.TCP_PROTOCOL, 0, null, 0);
    assertEquals("Source: 0.0.0.0 Dest: 192.168.0.1 Proto: 6 Id: 0 Data: [] Len: 0",
        packet.toString());
  }
  
  @Test
  public void testToStringNumbers() {
    byte[] data = { 4, 8, 15, 16, 23, 42 };
    IP.Packet packet = new IP.Packet(192 | 168 << 8 | 1 << 24, IP.TCP_PROTOCOL, 0, data, data.length);
    assertEquals("Source: 0.0.0.0 Dest: 192.168.0.1 Proto: 6 Id: 0 Data: [4,8,15,16,23,42] Len: 6",
        packet.toString());
  }
}
