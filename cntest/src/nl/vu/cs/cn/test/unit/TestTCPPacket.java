package nl.vu.cs.cn.test.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.ByteBuffer;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.IPUtil;
import nl.vu.cs.cn.tcp.Checksum;
import nl.vu.cs.cn.tcp.InvalidPacketException;
import nl.vu.cs.cn.tcp.TCPPacket;
import nl.vu.cs.cn.tcp.TCPPacket.FLAGS;
import nl.vu.cs.cn.tcp.TCPPacket.OFFSET;

import org.junit.Test;

public class TestTCPPacket {
  /**
   * Tests object construction of 'empty' TCPPacket.
   * @throws IOException 
   */
  
  @Test
  public void testEmptyTCPPacket() {
    short srcPort = 12345;
    short dstPort = (short)-1;
    TCPPacket tcpPacket = new TCPPacket(true, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    assertEquals(0, tcpPacket.getSeqNumber());
    assertEquals(0, tcpPacket.getAckNumber());
    assertEquals("0.0.0.0", tcpPacket.getSrcAddress().toString());
    assertEquals("0.0.0.0", tcpPacket.getDstAddress().toString());
    assertEquals("", tcpPacket.getFlagsString());
    assertEquals(false, tcpPacket.hasAck());
    assertEquals(false, tcpPacket.hasSyn());
    assertEquals(false, tcpPacket.hasFin());
    assertEquals(false, tcpPacket.hasPsh());
    assertEquals(false, tcpPacket.hasRst());
    assertEquals("{ src: 0.0.0.0, dst: 0.0.0.0, flg: , seq: 0, ack: 0, len: 20 }", tcpPacket.toString());
    
    try {
      tcpPacket.encode(0, 0, (byte)0, null);
      tcpPacket.decode();
      fail("Expected decode() to throw InvalidPacketException");
    }
    catch(InvalidPacketException e) {
      assertEquals("ACK flag should be set on each non-SYN packet", e.getMessage());
    }
  }
  
  @Test
  public void testSynPacket() {
    short srcPort = 12345;
    short dstPort = (short)-1;
    
    TCPPacket tcpPacket = new TCPPacket(true, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_SYN, null);

    assertEquals(23, tcpPacket.getSeqNumber());
    assertEquals(42, tcpPacket.getAckNumber());
    assertEquals("0.0.0.0", tcpPacket.getSrcAddress().toString());
    assertEquals("0.0.0.0", tcpPacket.getDstAddress().toString());
    assertEquals("SP", tcpPacket.getFlagsString());
    assertEquals(false, tcpPacket.hasAck());
    assertEquals(true, tcpPacket.hasSyn());
    assertEquals(false, tcpPacket.hasFin());
    assertEquals(true, tcpPacket.hasPsh());
    assertEquals(false, tcpPacket.hasRst());
    assertEquals("{ src: 0.0.0.0, dst: 0.0.0.0, flg: SP, seq: 23, ack: 42, len: 20 }", tcpPacket.toString());
    
    try {
      tcpPacket.decode();
    }
    catch(InvalidPacketException e) {
      fail("Invalid packet: " + e.getMessage());
    }
  }
  
  @Test
  public void testSynAckPacket() {
    short srcPort = 12345;
    short dstPort = (short)54321;
    IpAddress dstAddress = IpAddress.getAddress("192.168.0.250");
    TCPPacket tcpPacket = new TCPPacket(true, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.setDstAddress(dstAddress);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_SYN_ACK, null);
    
    assertEquals(23, tcpPacket.getSeqNumber());
    assertEquals(42, tcpPacket.getAckNumber());
    assertEquals("0.0.0.0", tcpPacket.getSrcAddress().toString());
    assertEquals("192.168.0.250", tcpPacket.getDstAddress().toString());
    assertEquals("SAP", tcpPacket.getFlagsString());
    assertEquals(true, tcpPacket.hasAck());
    assertEquals(true, tcpPacket.hasSyn());
    assertEquals(false, tcpPacket.hasFin());
    assertEquals(true, tcpPacket.hasPsh());
    assertEquals(false, tcpPacket.hasRst());
    assertEquals("{ src: 0.0.0.0, dst: 192.168.0.250, flg: SAP, seq: 23, ack: 42, len: 20 }", tcpPacket.toString());
    
    try {
      tcpPacket.decode();
    }
    catch(InvalidPacketException e) {
      fail("Invalid packet: " + e.getMessage());
    }
  }
  
  @Test
  public void testDataPacket() {
    byte[] data = { 4, 8, 15, 16, 23, 42 };
    ByteBuffer tcpPayload = ByteBuffer.wrap(data);
    short srcPort = 12345;
    short dstPort = (short)54321;
    IpAddress dstAddress = IpAddress.getAddress("192.168.0.250");
    TCPPacket tcpPacket = new TCPPacket(true, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.setDstAddress(dstAddress);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_SYN_ACK, tcpPayload);

    assertEquals(23, tcpPacket.getSeqNumber());
    assertEquals(42, tcpPacket.getAckNumber());
    assertEquals("0.0.0.0", tcpPacket.getSrcAddress().toString());
    assertEquals("192.168.0.250", tcpPacket.getDstAddress().toString());
    assertEquals("SAP", tcpPacket.getFlagsString());
    assertEquals(true, tcpPacket.hasAck());
    assertEquals(true, tcpPacket.hasSyn());
    assertEquals(false, tcpPacket.hasFin());
    assertEquals(true, tcpPacket.hasPsh());
    assertEquals(false, tcpPacket.hasRst());
    assertEquals("{ src: 0.0.0.0, dst: 192.168.0.250, flg: SAP, seq: 23, ack: 42, len: 26 }", tcpPacket.toString());
    
    try {
      tcpPacket.decode();
    }
    catch(InvalidPacketException e) {
      fail("Invalid packet: " + e.getMessage());
    }
  }
  
  @Test
  public void testLogging1() {
    byte[] data = { 4, 8, 15, 16, 23, 42 };
    ByteBuffer tcpPayload = ByteBuffer.wrap(data);
    short srcPort = 12345;
    short dstPort = (short)54321;
    IpAddress dstAddress = IpAddress.getAddress("192.168.0.250");
    TCPPacket tcpPacket = new TCPPacket(true, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.setDstAddress(dstAddress);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_SYN_ACK, tcpPayload);

    assertEquals("PACKET |     <-- SAP | src: 0.0.0.0:12345 | dst: 192.168.0.250:-11215 | seq: 23, ack: 42 | payload: 6B",
        tcpPacket.log(true));
  }
  
  @Test
  public void testLogging2() {
    byte[] data = { 4, 8, 15, 16, 23, 42 };
    ByteBuffer tcpPayload = ByteBuffer.wrap(data);
    short srcPort = 12345;
    short dstPort = (short)54321;
    IpAddress dstAddress = IpAddress.getAddress("192.168.0.250");
    TCPPacket tcpPacket = new TCPPacket(false, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.setDstAddress(dstAddress);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_FIN_ACK, tcpPayload);

    assertEquals("PACKET | FAP <--     | src: 0.0.0.0:12345 | dst: 192.168.0.250:-11215 | seq: 23, ack: 42 | payload: 6B",
        tcpPacket.log(false));
  }
  
  @Test
  public void testLogging3() {
    short srcPort = 12345;
    short dstPort = (short)54321;
    IpAddress dstAddress = IpAddress.getAddress("192.168.0.250");
    TCPPacket tcpPacket = new TCPPacket(true, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.setDstAddress(dstAddress);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_FIN_ACK, null);

    assertEquals("PACKET |     --> FAP | src: 0.0.0.0:12345 | dst: 192.168.0.250:-11215 | seq: 23, ack: 42",
        tcpPacket.log(false));
  }
  
  @Test
  public void testLogging4() {
    short srcPort = 12345;
    short dstPort = (short)54321;
    IpAddress dstAddress = IpAddress.getAddress("192.168.0.250");
    TCPPacket tcpPacket = new TCPPacket(false, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.setDstAddress(dstAddress);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_ACK, null);

    assertEquals("PACKET |  AP -->     | src: 0.0.0.0:12345 | dst: 192.168.0.250:-11215 | seq: 23, ack: 42",
        tcpPacket.log(true));
  }
  
  @Test
  public void testWrongChecksum() {
    TCPPacket packet = new TCPPacket(false, null, null, (short)1234, IpAddress.getAddress(0), (short)4321);
    try {
      packet.decode();
      fail("Expected packet.decode() to throw InvalidPacketException");
    }
    catch(InvalidPacketException e) {
      assertEquals("checksum was wrong: received=0 computed=-27",
          e.getMessage());
    }
  }
  
  @Test
  public void testRstPacket() {
    TCPPacket packet = new TCPPacket(false, null, null, (short)0, null, (short)0);
    packet.encode(0, 0, FLAGS.RST, null);
    assertTrue(packet.hasRst());
    assertEquals("{ src: 0.0.0.0, dst: 0.0.0.0, flg: R, seq: 0, ack: 0, len: 20 }", packet.toString());
  }
  
  @Test
  public void testSettersAndGetters() {
    TCPPacket packet = new TCPPacket(false, null, null, (short)0, null, (short)0);
    packet.setSrcAddress(IpAddress.getAddress("1.2.3.4"));
    packet.setSrcPort((short)1000);
    packet.setDstAddress(IpAddress.getAddress("5.6.7.8"));
    packet.setDstPort((short)2000);
    
    assertEquals(1 | 2 << 8 | 3 << 16 | 4 << 24, packet.getSrcAddress().getAddress());
    assertEquals(1000, packet.getSrcPort());
    assertEquals(5 | 6 << 8 | 7 << 16 | 8 << 24, packet.getDstAddress().getAddress());
    assertEquals(2000, packet.getDstPort());
  }
  
  @Test
  public void testDoubleEncode() {
    byte[] data1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    byte[] data2 = { 4, 8, 15, 16, 23, 42 };
    TCPPacket packet = new TCPPacket(false, null, null, (short)-1, null, (short)-1);
    packet.encode(100, 200, FLAGS.SEND_ACK, ByteBuffer.wrap(data1));
    
    assertEquals("{ src: 0.0.0.0, dst: 0.0.0.0, flg: AP, seq: 100, ack: 200, len: 30 }", packet.toString());
    
    packet.encode(300, 400, FLAGS.SEND_ACK, ByteBuffer.wrap(data2));
    
    assertEquals("{ src: 0.0.0.0, dst: 0.0.0.0, flg: AP, seq: 300, ack: 400, len: 26 }", packet.toString());
  }
  
  @Test
  public void testDecodeSynFin() {
    TCPPacket packet = new TCPPacket(false, null, null, (short)-1, null, (short)-1);
    packet.encode(0, 0, (byte)(FLAGS.SYN | FLAGS.FIN), null);
    try {
      packet.decode();
      fail("Expected decode() to throw InvalidPacketException");
    }
    catch(InvalidPacketException e) {
      assertEquals("SYN and FIN both set", e.getMessage());
    }
  }
  
  @Test
  public void testWrongSrcAddress() {
    // create packet
    TCPPacket packet = new TCPPacket(false, null, IpAddress.getAddress(1), (short)-1, null, (short)-1);
    packet.encode(0, 0, FLAGS.SEND_ACK, null);
    
    // change src address
    IPUtil.setSrcAddress(packet.getIpPacket(), IpAddress.getAddress(2));
    
    // fix checksum
    ByteBuffer data = IPUtil.getIpDataBuffer(packet.getIpPacket());
    int checksum = Checksum.compute(IpAddress.getAddress(2), IpAddress.getAddress(0), data);
    data.putShort(OFFSET.CHECKSUM, (short)checksum);
    
    try {
      packet.decode();
      fail("Expected InvalidPacketException");
    }
    catch(InvalidPacketException e) {
      assertEquals("Src address expected: 1.0.0.0, got: 2.0.0.0", e.getMessage());
    }
  }
  
  @Test
  public void testWrongDstAddress() {
    // create packet
    TCPPacket packet = new TCPPacket(false, null, null, (short)-1, IpAddress.getAddress(1), (short)-1);
    packet.encode(0, 0, FLAGS.SEND_ACK, null);
    
    // change dst address
    IPUtil.setDstAddress(packet.getIpPacket(), IpAddress.getAddress(2));
    
    // fix checksum
    ByteBuffer data = IPUtil.getIpDataBuffer(packet.getIpPacket());
    int checksum = Checksum.compute(IpAddress.getAddress(0), IpAddress.getAddress(2), data);
    data.putShort(OFFSET.CHECKSUM, (short)checksum);
    
    try {
      packet.decode();
      fail("Expected InvalidPacketException");
    }
    catch(InvalidPacketException e) {
      assertEquals("Dst address expected: 1.0.0.0, got: 2.0.0.0", e.getMessage());
    }
  }
  
  @Test
  public void testWrongSrcPort() {
    // create packet
    TCPPacket packet = new TCPPacket(false, null, null, (short)1234, null, (short)-1);
    packet.encode(0, 0, FLAGS.SEND_ACK, null);
    ByteBuffer data = IPUtil.getIpDataBuffer(packet.getIpPacket());
    
    // change src port
    
    data.putShort(OFFSET.SRC_PORT, (short)4321);
    
    // fix checksum
    int checksum = Checksum.compute(IpAddress.getAddress(0), IpAddress.getAddress(0), data);
    data.putShort(OFFSET.CHECKSUM, (short)checksum);
    
    try {
      packet.decode();
      fail("Expected InvalidPacketException");
    }
    catch(InvalidPacketException e) {
      assertEquals("Src port expected: 1234, got: 4321", e.getMessage());
    }
  }
  
  @Test
  public void testWrongDstPort() {
    // create packet
    TCPPacket packet = new TCPPacket(false, null, null, (short)-1, null, (short)1234);
    packet.encode(0, 0, FLAGS.SEND_ACK, null);
    ByteBuffer data = IPUtil.getIpDataBuffer(packet.getIpPacket());
    
    // change dst port
    
    data.putShort(OFFSET.DST_PORT, (short)4321);
    
    // fix checksum
    int checksum = Checksum.compute(IpAddress.getAddress(0), IpAddress.getAddress(0), data);
    data.putShort(OFFSET.CHECKSUM, (short)checksum);
    
    try {
      packet.decode();
      fail("Expected InvalidPacketException");
    }
    catch(InvalidPacketException e) {
      assertEquals("Dst port expected: 1234, got: 4321", e.getMessage());
    }
  }
  
  @Test
  public void testLearnDstPort() {
    short srcPort = 12345;
    short dstPort = (short)-1;
    
    TCPPacket tcpPacket = new TCPPacket(true, null, IpAddress.getAddress(0), srcPort, null, dstPort);
    tcpPacket.encode(23, 42, TCPPacket.FLAGS.SEND_SYN, null);
    
    // change port from "don't know" to 1000
    ByteBuffer data = IPUtil.getIpDataBuffer(tcpPacket.getIpPacket());
    data.putShort(OFFSET.DST_PORT, (short)1000);
    data.putShort(OFFSET.CHECKSUM, Checksum.compute(IpAddress.getAddress(0), IpAddress.getAddress(0), data));
    
    assertEquals(1000, tcpPacket.getDstPort());
    
    try {
      tcpPacket.decode();
    }
    catch(InvalidPacketException e) {
      fail("Invalid packet: " + e.getMessage());
    }
  }
  
  @Test
  public void testWindowSize() {
    TCPPacket packet = new TCPPacket(false, null, null, (short)-1, null, (short)-1);
    packet.encode(0, 0, (byte)0, null);
    ByteBuffer ipPayload = IPUtil.getIpDataBuffer(packet.getIpPacket());
    assertEquals(7960, ipPayload.getShort(OFFSET.WINDOW_SIZE));
  }
}
