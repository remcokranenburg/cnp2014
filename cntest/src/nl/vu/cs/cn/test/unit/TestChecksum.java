package nl.vu.cs.cn.test.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.tcp.Checksum;

import org.junit.Test;

public class TestChecksum {
  
  @Test
  public void verifyFalse() {
    IpAddress srcAddress = IpAddress.getAddress("255.0.0.0");
    IpAddress dstAddress = IpAddress.getAddress("0.228.0.0");
    ByteBuffer tcpData = ByteBuffer.allocate(20);

    assertEquals(1, Checksum.compute(srcAddress, dstAddress, tcpData));
    
    System.out.println("In this test, we expect checksums not to match.");
    assertFalse(Checksum.verify(srcAddress, dstAddress, tcpData));
  }
  
  @Test
  public void verifyTrue() {
    IpAddress srcAddress = IpAddress.getAddress("255.229.0.0");
    IpAddress dstAddress = IpAddress.getAddress(0);
    ByteBuffer tcpData = ByteBuffer.allocate(20);
    
    assertEquals(0, Checksum.compute(srcAddress, dstAddress, tcpData));
    assertTrue(Checksum.verify(srcAddress, dstAddress, tcpData));
  }
  
  @Test
  public void computeEmpty() {
    IpAddress srcAddress = IpAddress.getAddress(0);
    IpAddress dstAddress = IpAddress.getAddress(0);
    ByteBuffer tcpData = ByteBuffer.allocate(20);
    
    assertEquals(-27, Checksum.compute(srcAddress, dstAddress, tcpData));
  }
  
  /**
   * Test checksumming of a packet with an odd number of bytes
   */
  
  @Test
  public void computeWithPadding() {
    IpAddress srcAddress = IpAddress.getAddress("255.228.0.0");
    IpAddress dstAddress = IpAddress.getAddress(0);
    ByteBuffer tcpData = ByteBuffer.allocate(21);
    
    assertEquals(0, Checksum.compute(srcAddress, dstAddress, tcpData));
    assertTrue(Checksum.verify(srcAddress, dstAddress, tcpData));
  }
}
