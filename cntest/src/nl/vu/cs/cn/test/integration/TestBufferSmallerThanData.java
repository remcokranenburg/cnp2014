package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;
import android.test.AndroidTestCase;

public class TestBufferSmallerThanData extends AndroidTestCase {
	public void testBufferSmallerThanData() {
		final boolean[] done = { false };
		try {
			final String srcString = "4815234248152342";
			final byte[] srcData = srcString.getBytes();
			final byte[] destData = new byte[8];
			
			final TCP serverTcp = new TCP(1);
			final Socket serverSocket = serverTcp.socket(50000);
			
			final TCP clientTcp = new TCP(2);
			final Socket clientSocket = clientTcp.socket();
			
			Thread serverThread = new Thread() {
				public void run()
				{
					serverSocket.accept();
					System.out.println("Connection accepted");
					assertEquals(8, serverSocket.read(destData, 0, 16));
					assertTrue("48152342".compareTo(new String(destData)) == 0);
					done[0] = true;
					System.out.println("Data read");
					serverSocket.close();
				}
			};
			serverThread.start();
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000);
			assertEquals(16, clientSocket.write(srcData, 0, 32));
			clientSocket.close();
			
			new Thread() {
				public void run() {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					assertTrue(done[0]);
				}
			}.start();
			
			serverThread.join();
		}
		catch(IOException e) {
			fail(e.getMessage());
		} catch (InterruptedException e) {
			fail(e.getMessage());
		}
	}
}
