package nl.vu.cs.cn.test.integration;

import java.util.Random;

import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;
import android.test.AndroidTestCase;

public class TestBigData extends AndroidTestCase {
  private volatile AssertionError exc;
  
  public void testBigData() throws Exception {
		Random r = new Random(42);
		final byte[] srcData = new byte[16000];
		r.nextBytes(srcData);
		
		final byte[] destData = new byte[16000];
		
		final TCP serverTcp = new TCP(1);
		final Socket serverSocket = serverTcp.socket(50000);
		
		final TCP clientTcp = new TCP(2);
		final Socket clientSocket = clientTcp.socket();
		
		Thread serverThread = new Thread() {
			public void run()
			{
			  try {
  				serverSocket.accept();
  				System.out.println("Connection accepted");
  				
  				assertEquals(destData.length, serverSocket.read(destData, 0, destData.length));
  				
  				for(int i = 0; i < srcData.length; i++) {
  					assertEquals(srcData[i], destData[i]);
  				}
  				System.out.println("Data read");
  				serverSocket.close();
			  }
  			catch(AssertionError e) {
  			  exc = e;
  			}
			  catch(Exception e) {
			    e.printStackTrace();
			  }
			}
		};
		serverThread.start();
		
		boolean connected = clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000);

    if(exc != null) throw exc;
		assertTrue("Expected client to be connected", connected);
		
		assertEquals(16000, clientSocket.write(srcData, 0, srcData.length));
		clientSocket.close();
		
		try {
      serverThread.join(2000);
    }
    catch(InterruptedException e) {
      fail("Expected server thread to have finished after 2 seconds");
    }
		
		if(exc != null) throw exc;
	}
}
