package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import org.junit.Ignore;

import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;
import android.test.AndroidTestCase;

public class TestRequestMoreThanReceived extends AndroidTestCase {
  private volatile AssertionError exc;
  
  /**
   * This test is wrong. I was under the impression that reads should also return after 10 retransmissions. The test 
   * is ignored.
   */
  
  @Ignore
	private void testRequestMoreThanReceived() {
		
		try {
			final String srcString = "4815234248152342";
			final byte[] srcData = srcString.getBytes();
			final byte[] destData = new byte[32];
			
			final TCP serverTcp = new TCP(1);
			final Socket serverSocket = serverTcp.socket(50000);
			
			final TCP clientTcp = new TCP(2);
			final Socket clientSocket = clientTcp.socket();
			
			Thread serverThread = new Thread() {
				public void run()
				{
				  try {
  					serverSocket.accept();
  					System.out.println("Connection accepted");
  					assertEquals(8, serverSocket.read(destData, 0, 32));
  					assertEquals("48152342", new String(destData).substring(0, 8));
  					System.out.println("Data read");
  					serverSocket.close();
				  }
				  catch(AssertionError e) {
				    exc = e;
				  }
				}
			};
			serverThread.start();
			
			clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000);
      if(exc != null) throw exc;
			assertTrue(8 == clientSocket.write(srcData, 0, 8));
			clientSocket.close();
			
			serverThread.join();
			if(exc != null) throw exc;
		}
		catch(IOException e) {
			fail(e.getMessage());
		} catch (InterruptedException e) {
			fail(e.getMessage());
		}
	}
}
