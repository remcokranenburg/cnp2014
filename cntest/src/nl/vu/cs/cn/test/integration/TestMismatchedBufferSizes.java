package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;
import android.test.AndroidTestCase;

public class TestMismatchedBufferSizes extends AndroidTestCase {
  private volatile AssertionError exc;
  private volatile boolean done = false;
  
	public void testBigToSmall() {
		try {
			final String srcString = "4815234248152342";
			final byte[] srcData = srcString.getBytes();
			final byte[] destData = new byte[8];
			
			final TCP serverTcp = new TCP(1);
			final Socket serverSocket = serverTcp.socket(50000);
			
			final TCP clientTcp = new TCP(2);
			final Socket clientSocket = clientTcp.socket();
			
			Thread serverThread = new Thread() {
				public void run()
				{
				  try {
  					serverSocket.accept();
  					System.out.println("Connection accepted");
  					serverSocket.read(destData, 0, 8);
  					assertTrue("48152342".compareTo(new String(destData)) == 0);
  					serverSocket.read(destData, 0, 8);
  					assertTrue("48152342".compareTo(new String(destData)) == 0);
  					System.out.println("Data read");
  					done = true;
  					serverSocket.close();
				  }
				  catch(AssertionError e) {
				    exc = e;
				  }
				}
			};
			serverThread.start();
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			boolean connected = clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000);

	    assertTrue("Expected client to be connected", connected);
			
			clientSocket.write(srcData, 0, srcData.length);
			clientSocket.close();
			
			serverThread.join(2000);
			if(exc != null) throw exc;
			assertTrue("Server thread should be completed", done);
		}
		catch(IOException e) {
			fail(e.getMessage());
		} catch (InterruptedException e) {
			fail(e.getMessage());
		}
	}
	
	public void testSmallToBig() {
		System.out.println("--------------------------------");
		System.out.println("testSmallToBig");
		System.out.println("--------------------------------");
		
		try {
			final String srcString = "4815234248152342";
			final byte[] srcData = srcString.getBytes();
			final byte[] destData = new byte[16];
			
			final TCP serverTcp = new TCP(1);
			final Socket serverSocket = serverTcp.socket(50000);
			
			final TCP clientTcp = new TCP(2);
			final Socket clientSocket = clientTcp.socket();
			
			Thread serverThread = new Thread() {
				public void run()
				{
					serverSocket.accept();
					System.out.println("Connection accepted");
					assertEquals(9, serverSocket.write(srcData, 0, 9));
					assertEquals(7, serverSocket.write(srcData, 9, 7));
					serverSocket.close();
				}
			};
			serverThread.start();
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000);
			assertEquals(7, clientSocket.read(destData, 0, 7));
			assertEquals(9, clientSocket.read(destData, 7, 9));
      assertEquals(srcString, new String(destData));
			clientSocket.close();
			
			serverThread.join(15000);
		}
		catch(IOException e) {
			fail(e.getMessage());
		}
		catch(InterruptedException e) {
			fail(e.getMessage());
		}
	}
}
