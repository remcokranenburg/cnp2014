package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import android.test.AndroidTestCase;

public class TestSeqWrap extends AndroidTestCase {
  
  @Before
  public void setUp() {
    System.setProperty("debug.seq", "1");
  }
  
  @After
  public void tearDown() {
    System.clearProperty("debug.seq");
  }
  
  @Test
  public void testSeqWrap() {
    Runnable server = new Runnable() {
      @Override
      public void run() {
        try {
          TCP tcp = new TCP(1);
          Socket socket = tcp.socket(1337);
          socket.accept();
        }
        catch(IOException e) {
          fail(e.getMessage());
        }
      }
    };

    Thread serverThread = new Thread(server);
    serverThread.start();
    
    try {
      TCP tcp = new TCP(2);
      Socket socket = tcp.socket();
      boolean connected = socket.connect(IpAddress.getAddress("192.168.0.1"), 1337);
      assertTrue("Expect client to be connected", connected);
    }
    catch(IOException e) {
      fail(e.getMessage());
    }
    
    try {
      serverThread.join(2000);
    }
    catch(InterruptedException e) {
      fail("Expected server to have accepted the connection after two seconds");
    }
  }
  
}
