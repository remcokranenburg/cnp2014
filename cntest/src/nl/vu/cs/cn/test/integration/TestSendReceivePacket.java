package nl.vu.cs.cn.test.integration;

import java.io.IOException;
import java.nio.ByteBuffer;

import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.tcp.InvalidPacketException;
import nl.vu.cs.cn.tcp.TCPPacket;
import nl.vu.cs.cn.tcp.TCPPacket.FLAGS;

import org.junit.Test;

import android.test.AndroidTestCase;

public class TestSendReceivePacket extends AndroidTestCase {
  private volatile AssertionError exc;
  
  public void setUp() {
    exc = null;
  }
  
  @Test
  public void testSendReceiveNumberPacket() throws IOException {
    final IP ipA = new IP(1);
    final IP ipB = new IP(2);
    
    final byte[] data = { 4, 8, 15, 16, 23, 42 };
    final short srcPort = 12345;
    final short dstPort = (short)54321;
    
    Thread sendThread = new Thread(new Runnable() {
      public void run() {        
        TCPPacket sendTcpPacket = new TCPPacket(true, ipA, ipA.getLocalAddress(), srcPort, ipB.getLocalAddress(), dstPort);
        try {
          sendTcpPacket.send(0, 0, FLAGS.SEND_ACK, ByteBuffer.wrap(data));
        }
        catch(IOException e) {
          e.printStackTrace();
          fail("Sending failed: " + e.getMessage());
        }
      }
    });
    sendThread.start();
    
    TCPPacket receiveTcpPacket = new TCPPacket(false, ipB, null, srcPort, null, dstPort);
    
    try {
      receiveTcpPacket.receive(1);
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail("Receiving timeout: " + e.getMessage());
    }
    catch(IOException e) {
      e.printStackTrace();
      fail("Receiving failed: " + e.getMessage());
    }
    catch(InvalidPacketException e) {
      e.printStackTrace();
      fail("Received invalid packet: " + e.getMessage());
    }
    
    try {
      sendThread.join(2000);
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail("Sending timeout: " + e.getMessage());
    }
    
    ByteBuffer received = receiveTcpPacket.getPayload();
    
    for(int i = 0; i < data.length; i++) {
      assertEquals(data[i], received.get());
    }
  }
}
