package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import org.junit.Before;

import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;
import android.test.AndroidTestCase;

public class TestSimultaneousReadAndWrite extends AndroidTestCase {
  private volatile AssertionError exc;
  
  @Before
  public void setUp() {
    exc = null;
  }
  
  public void testSimultaneousReadAndWrite() {
    System.out.println("--------------------------------");
    System.out.println("testSimultaneousReadAndWrite");
    System.out.println("--------------------------------");
    
    try {
      final TCP serverTcp = new TCP(1);
      final Socket serverSocket = serverTcp.socket(50000);
      
      final TCP clientTcp = new TCP(2);
      final Socket clientSocket = clientTcp.socket();
      
      final byte[] sentByClient = { 4, 8, 15, 16, 23, 42 };
      final byte[] sentByServer = { 42, 23, 16, 15, 8, 4 };
      final byte[] receivedByClient = new byte[sentByServer.length];
      final byte[] receivedByServer = new byte[receivedByClient.length];
      
      Thread serverThread = new Thread() {
        public void run()
        {
          try {
            serverSocket.accept();
            assertEquals(receivedByServer.length, serverSocket.read(receivedByServer, 0, receivedByServer.length));
            assertEquals(sentByServer.length, serverSocket.write(sentByServer, 0, sentByServer.length));
            assertTrue(serverSocket.close());
          }
          catch(AssertionError e) {
            exc = e;
          }
        }
      };
      
      Thread clientThread = new Thread() {
        /*
         * The client should read and write at the same time, so reading is done in a new thread
         */
        
        Thread clientReadThread = new Thread() {
          public void run() {
            try {
              assertEquals(receivedByClient.length, clientSocket.read(receivedByClient, 0, receivedByClient.length));
            }
            catch(AssertionError e) {
              exc = e;
            }
          }
        };
        
        public void run()
        {
          try {
            assertTrue(clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000));
            clientReadThread.start(); // start reading
            Thread.sleep(2000);
            assertEquals(sentByClient.length, clientSocket.write(sentByClient, 0, sentByClient.length));
            assertTrue(clientSocket.close());
          }
          catch(AssertionError e) {
            exc = e;
          }
          catch(InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      };
      
      serverThread.start();
      
      if(exc != null) throw exc;
      
      clientThread.start();
      
      if(exc != null) throw exc;
      
      serverThread.join(2000);
      
      if(exc != null) throw exc;
      
      clientThread.join(2000);
      
      if(exc != null) throw exc;
    }
    catch(IOException e) {
      fail(e.getMessage());
    }
    catch (InterruptedException e) {
      fail(e.getMessage());
    }
  }
}