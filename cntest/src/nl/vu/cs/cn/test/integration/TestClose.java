package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.TCP.Socket;
import android.test.AndroidTestCase;

public class TestClose extends AndroidTestCase {
  volatile boolean closeReturned;
  volatile boolean closed;
  volatile boolean connectReturned;
  volatile boolean connected;
  volatile boolean acceptReturned;
  volatile boolean readReturned;
  volatile AssertionError exc;

  public void setUp() {
    closeReturned = false;
    closed = false;
    connectReturned = false;
    connected = false;
    acceptReturned = false;
    exc = null;
    readReturned = false;
  }
  
  public void testCloseWhileClosedClient() {
    try {
      TCP tcp = new TCP(1);
      Socket socket = tcp.socket();
      assertFalse(socket.close());
    }
    catch(IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
  
  public void testCloseWhileClosedServer() {
    try {
      TCP tcp = new TCP(1);
      Socket socket = tcp.socket(12345);
      assertFalse(socket.close());
    }
    catch(IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
  
  public void testCloseWhileConnecting() {
    try {
      TCP tcp = new TCP(1);
      final Socket socket = tcp.socket();
      
      Thread closeThread = new Thread() {
        public void run() {
          try {
            Thread.sleep(500);
          }
          catch(InterruptedException e) {}
          
          closed = socket.close();
          closeReturned = true;
        }
      };
      
      Thread connectThread = new Thread() {
        public void run() {
          connected = socket.connect(IpAddress.getAddress("192.168.0.2"), 12345);
          connectReturned = true;
        }
      };
      
      closeThread.start();
      connectThread.start();
      
      Thread.sleep(2000);
      
      assertTrue(connectReturned);
      assertFalse(connected);
      assertTrue(closeReturned);
      assertFalse(closed);
    }
    catch(IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
  
  public void testServerCloseWhileAccepting() {
    try {
      final TCP tcp = new TCP(1);
      final Socket socket = tcp.socket(12345);
      
      Thread acceptThread = new Thread() {
        public void run() {
          socket.accept();
          acceptReturned = true;
        }
      };
      
      acceptThread.start();
      
      Thread.sleep(500);
      
      boolean closed = socket.close();
      
      Thread.sleep(500);
      
      assertFalse(acceptReturned);
      assertFalse(closed);
    }
    catch(IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
  
  public void testCloseWhileEstablished() {
    try {
      final TCP serverTcp = new TCP(1);
      final TCP clientTcp = new TCP(2);
      
      Thread serverThread = new Thread() {
        public void run() {
          Socket socket = serverTcp.socket(1234);
          socket.accept();
          assertTrue(socket.close());
        }
      };
      
      Thread clientThread = new Thread() {
        public void run() {
          Socket socket = clientTcp.socket();
          assertTrue(socket.connect(IpAddress.getAddress("192.168.0.1"), 1234));
          assertTrue(socket.close());
        }
      };
      serverThread.start();
      clientThread.start();
      
      serverThread.join();
      clientThread.join();

      if(exc != null) throw exc;
    }
    catch(IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
  
  public void testCloseWhileReading() {
    try {
      final TCP serverTcp = new TCP(1);
      final TCP clientTcp = new TCP(2);
      
      Thread serverThread = new Thread() {
        public void run() {
          Socket socket = serverTcp.socket(1234);
          socket.accept();
          byte[] buf = new byte[6];
          assertEquals(0, socket.read(buf, 0, buf.length));
          readReturned = true;
          assertTrue(socket.close());
        }
      };
      
      Thread clientThread = new Thread() {
        public void run() {
          Socket socket = clientTcp.socket();
          assertTrue(socket.connect(IpAddress.getAddress("192.168.0.1"), 1234));
          assertTrue(socket.close());
        }
      };
      
      serverThread.start();
      clientThread.start();
      
      Thread.sleep(2000);
      
      assertTrue(readReturned);
      
      if(exc != null) throw exc;
    }
    catch(IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
  
  public void testActiveAndPassiveClose() {
    try {
      final TCP serverTcp = new TCP(1);
      final TCP clientTcp = new TCP(2);
      
      Thread serverThread = new Thread() {
        public void run() {
          try {
            Socket socket = serverTcp.socket(1234);
            socket.accept();

            try {
              Thread.sleep(1000);
            }
            catch(InterruptedException e) {}
            
            assertTrue(socket.close());
            
            try {
              Thread.sleep(1000);
            }
            catch(InterruptedException e) {}
            
            byte[] someBytes = { 4, 8, 15, 16, 23, 42 };
            assertEquals(-1, socket.write(someBytes, 0, someBytes.length));
            
            byte[] otherBytes = { 0, 0, 0, 0 ,0 ,0};
            socket.read(otherBytes, 0, 6);
          }
          catch(AssertionError e) {
            exc = e;
          }
        }
      };
      
      Thread clientThread = new Thread() {
        public void run() {
          try {
            Socket socket = clientTcp.socket();
            socket.connect(IpAddress.getAddress("192.168.0.1"), 1234);
            
            byte[] someBytes = { 4, 8, 15, 16, 23, 42 };
            assertEquals(6, socket.write(someBytes, 0, someBytes.length));
            
            assertTrue(socket.close());
          }
          catch(AssertionError e) {
            exc = e;
          }
        }
      };
      
      serverThread.start();
      clientThread.start();
      
      serverThread.join();
      clientThread.join();
      
      if(exc != null) throw exc;
    }
    catch(IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
}
