package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.TCP.Socket;

import org.junit.Before;
import org.junit.Test;

import android.test.AndroidTestCase;

public class TestConnect extends AndroidTestCase {
  private volatile AssertionError exc;
  private volatile boolean connectReturned;
  
  @Before
  public void resetExc() {
    exc = null;
    connectReturned = false;
  }
  
  @Test
  public void testSimpleConnect() {
    Runnable server = new Runnable() {
      @Override
      public void run() {
        try {
          TCP tcp = new TCP(1);
          Socket socket = tcp.socket(1337);
          socket.accept();
        }
        catch(IOException e) {
          fail(e.getMessage());
        }
      }
    };

    Thread serverThread = new Thread(server);
    serverThread.start();
    
    try {
      TCP tcp = new TCP(2);
      Socket socket = tcp.socket();
      boolean connected = socket.connect(IpAddress.getAddress("192.168.0.1"), 1337);
      assertTrue("Expect client to be connected", connected);
    }
    catch(IOException e) {
      fail(e.getMessage());
    }
    
    try {
      serverThread.join(2000);
    }
    catch(InterruptedException e) {
      fail("Expected server to have accepted the connection after two seconds");
    }
  }
  
  
  public void testAcceptBeforeConnect() {
    final boolean[] done = { false };
    try {
      final String srcString = "48152342";
      final byte[] srcData = srcString.getBytes();
      final byte[] destData = new byte[8];
      
      final TCP serverTcp = new TCP(1);
      final Socket serverSocket = serverTcp.socket(50000);
      
      final TCP clientTcp = new TCP(2);
      final Socket clientSocket = clientTcp.socket();
      
      Thread serverThread = new Thread() {
        public void run()
        {
          try {
            serverSocket.accept();
            System.out.println("Connection accepted");
            serverSocket.read(destData, 0, 8);
            final String destString = new String(destData);
            assertTrue(srcString.compareTo(destString) == 0);
            done[0] = true;
            System.out.println("Data read");
            serverSocket.close();
          }
          catch(AssertionError e) {
            exc = e;
          }
        }
      };
      serverThread.start();
      
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      
      clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000);
      clientSocket.write(srcData, 0, srcData.length);
      clientSocket.close();
      
      serverThread.join(2000);
      if(exc != null) throw exc;
    }
    catch(IOException e) {
      fail(e.getMessage());
    } catch (InterruptedException e) {
      fail(e.getMessage());
    }
  }
  
public void testTcpConnectBeforeAccept() {
    
    try {
      final TCP serverTcp = new TCP(1);
      final Socket serverSocket = serverTcp.socket(50000);
      
      final TCP clientTcp = new TCP(2);
      final Socket clientSocket = clientTcp.socket();
      
      // connect before accept
      Thread clientThread = new Thread() {
        public void run() {
          try {
            boolean connected = clientSocket.connect(IpAddress.getAddress("192.168.0.1"), 50000);
            assertTrue(connected);
          }
          catch(AssertionError e) {
            exc = e;
          }
        }
      };
      
      clientThread.start();
      
      // delay accept for 2 second
      Thread.sleep(2000);
      
      Thread serverThread = new Thread() {
        public void run() {
          try {
            serverSocket.accept();
          }
          catch(AssertionError e) {
            exc = e;
          }
        }
      };
      serverThread.start();
      
      clientThread.join(11000);
      serverThread.join(2000);
      if(exc != null) throw exc;
    }
    catch(IOException e) {
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      fail(e.getMessage());
    }
  }

  public void testConnectFailed() {
    try {
      new Thread() {
        public void run() {
          try {
            TCP tcp = null;
            try {
              tcp = new TCP(1);
            }
            catch(IOException e) {
              fail(e.getMessage());
            }
            Socket socket = tcp.socket();
            boolean connected = socket.connect(IpAddress.getAddress(0), 0);
            connectReturned = true;
            assertFalse(connected);
          }
          catch(AssertionError e) {
            exc = e;
          }
        }
      }.start();
      
      Thread.sleep(8000);
      
      assertFalse(connectReturned);
      
      Thread.sleep(4000);
      
      assertTrue(connectReturned);
      if(exc != null) throw exc;
    }
    catch(InterruptedException e) {
      fail(e.getMessage());
    }
  }
}
