package nl.vu.cs.cn.test.integration;

import java.io.IOException;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.TCP.Socket;

import org.junit.Before;
import org.junit.Test;

import android.test.AndroidTestCase;

public class TestReadWrite extends AndroidTestCase {
  private volatile AssertionError exc;
  private volatile boolean writeReturned;
  
  @Before
  public void resetExc() {
    exc = null;
    writeReturned = false;
  }
  
  @Test
  public void testClientWriteNumbers() {
    Runnable server = new Runnable() {
      @Override
      public void run() {
        try {
          TCP tcp = new TCP(1);
          Socket socket = tcp.socket(1337);
          socket.accept();

          byte[] expected = { 4, 8, 15, 16, 23, 42 };
          byte[] actual = { 0, 0, 0, 0, 0, 0 };
          int bytesReceived = socket.read(actual, 0, actual.length);
          
          assertEquals(expected.length, bytesReceived);
          System.out.println("Received " + bytesReceived + " bytes:");
          
          for(int i = 0; i < expected.length; i++) {
            System.out.println((int)actual[i]);
            assertEquals("Number", expected[i], actual[i]);
          }
          
          socket.close();
        }
        catch(AssertionError e) {
          exc = e;
        }
        catch(IOException e) {
          e.printStackTrace();
        }
      }
    };
    
    Thread serverThread = new Thread(server);
    serverThread.start();
    
    try {
      TCP tcp = new TCP(2);
      Socket socket = tcp.socket();
      boolean connected = socket.connect(IpAddress.getAddress("192.168.0.1"), 1337);
      assertTrue("Expect client to be connected", connected);
      
      byte[] buf = { 4, 8, 15, 16, 23, 42 };
      socket.write(buf, 0, buf.length);
      socket.close();
      
    }
    catch(IOException e) {
      fail(e.getMessage());
    }

    try {
      serverThread.join(2000);
      if(exc != null) throw exc;
    }
    catch(InterruptedException e) {
      fail("Expected server to have accepted the connection after two seconds");
    }
  }
  
  @Test
  public void testClientReadNumbers() {
    
    Runnable server = new Runnable() {
      @Override
      public void run() {
        try {
          
          TCP tcp = new TCP(1);
          Socket socket = tcp.socket(1337);
          socket.accept();

          byte[] buf = { 4, 8, 15, 16, 23, 42 };
          int bytesWritten = socket.write(buf, 0, buf.length);
          
          assertEquals(buf.length, bytesWritten);
        }
        catch(IOException e) {
          fail(e.getMessage());
        }
        catch(AssertionError e) {
          exc = e;
        }
      }
    };
    
    Thread serverThread = new Thread(server);
    serverThread.start();
    
    try {
      TCP tcp = new TCP(2);
      Socket socket = tcp.socket();
      boolean connected = socket.connect(IpAddress.getAddress("192.168.0.1"), 1337);
      assertTrue("Expect client to be connected", connected);

      byte[] expected = { 4, 8, 15, 16, 23, 42 };
      byte[] buf = new byte[expected.length];
      int bytesRead = socket.read(buf, 0, buf.length);

      if(exc != null) throw exc;
      assertEquals(buf.length, bytesRead);
      
      for(int i = 0; i < expected.length; i++) {
        assertEquals((int)expected[i], (int)buf[i]);
      }
    }
    catch(IOException e) {
      fail(e.getMessage());
    }

    try {
      serverThread.join(2000);
      if(exc != null) throw exc;
    }
    catch(InterruptedException e) {
      fail("Expected server to have accepted the connection after two seconds");
    }
  }
  
  @Test
  public void testDelayRead() {
    
    Runnable server = new Runnable() {
      @Override
      public void run() {
        try {
          
          TCP tcp = new TCP(1);
          Socket socket = tcp.socket(1337);
          socket.accept();

          byte[] buf = { 4, 8, 15, 16, 23, 42 };
          int bytesWritten = socket.write(buf, 0, buf.length);
          
          assertEquals(buf.length, bytesWritten);
        }
        catch(IOException e) {
          fail(e.getMessage());
        }
        catch(AssertionError e) {
          exc = e;
        }
      }
    };
    
    Thread serverThread = new Thread(server);
    serverThread.start();
    
    try {
      TCP tcp = new TCP(2);
      Socket socket = tcp.socket();
      boolean connected = socket.connect(IpAddress.getAddress("192.168.0.1"), 1337);
      assertTrue("Expect client to be connected", connected);

      byte[] expected = { 4, 8, 15, 16, 23, 42 };
      byte[] buf = new byte[expected.length];
      
      Thread.sleep(5000);
      
      int bytesRead = socket.read(buf, 0, buf.length);

      if(exc != null) throw exc;
      assertEquals(buf.length, bytesRead);
      
      for(int i = 0; i < expected.length; i++) {
        assertEquals((int)expected[i], (int)buf[i]);
      }
    }
    catch(IOException e) {
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      fail(e.getMessage());
    }

    try {
      serverThread.join(2000);
      if(exc != null) throw exc;
    }
    catch(InterruptedException e) {
      fail("Expected server to have accepted the connection after two seconds");
    }
  }
  
  public void testWriteFailed() {
    Runnable server = new Runnable() {
      @Override
      public void run() {
        try {
          
          TCP tcp = new TCP(1);
          Socket socket = tcp.socket(1337);
          socket.accept();

          byte[] buf = { 4, 8, 15, 16, 23, 42 };
          int bytesWritten = socket.write(buf, 0, buf.length);
          writeReturned = true;
          assertEquals(0, bytesWritten);
        }
        catch(IOException e) {
          fail(e.getMessage());
        }
        catch(AssertionError e) {
          exc = e;
        }
      }
    };
    
    Thread serverThread = new Thread(server);
    serverThread.start();
    
    try {
      TCP tcp = new TCP(2);
      Socket socket = tcp.socket();
      boolean connected = socket.connect(IpAddress.getAddress("192.168.0.1"), 1337);
      assertTrue("Expect client to be connected", connected);
      
      Thread.sleep(8000);
      
      assertFalse(writeReturned);
      
      Thread.sleep(4000);
      
      assertTrue(writeReturned);
      
      if(exc != null) throw exc;
    }
    catch(IOException e) {
      fail(e.getMessage());
    }
    catch(InterruptedException e) {
      fail(e.getMessage());
    }

    try {
      serverThread.join(2000);
      if(exc != null) throw exc;
    }
    catch(InterruptedException e) {
      fail("Expected server to have accepted the connection after two seconds");
    }
  }
}
