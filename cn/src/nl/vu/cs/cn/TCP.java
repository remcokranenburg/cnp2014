package nl.vu.cs.cn;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.tcp.ConnectionState;
import nl.vu.cs.cn.tcp.InvalidPacketException;
import nl.vu.cs.cn.tcp.TCPPacket;
import nl.vu.cs.cn.tcp.TCPPacket.FLAGS;

/**
 * This class represents a TCP stack. It should be built on top of the IP stack which is bound to a
 * given IP address.
 */

public class TCP {
  /** The underlying IP stack for this TCP stack. */
  private final IP ip;
  
  /**
   * This class represents a TCP socket.
   */
  
  public class Socket {
    private final boolean         IS_SERVER;
    private final ConnectionState state;
    private final TCPPacket       receivePacket;
    private final TCPPacket       sendPacket;
    
    private long                  ourSeq;
    private long                  theirSeq;
    
    private ByteBuffer            lastReceivedBuffer;
    
    /**
     * Construct a client socket.
     */
    
    private Socket() {
      this(new Random().nextInt(2 << 16), false);
    }
    
    /**
     * Construct a server socket bound to the given local port.
     * 
     * @param port the local port to use
     */
    
    private Socket(int port) {
      this(port, true);
    }
    
    /**
     * General constructor that is called by both the server constructor and the client constructor
     * 
     * @param port Our port number
     * @param isServer Determines whether the Socket should be constructed as server or as client
     */
    
    private Socket(int port, boolean isServer) {
      IS_SERVER = isServer;
      short ourPort = (short)port;
      short theirPort = -1; // set to -1 meaning 'unknown'
      
      state = new ConnectionState(ConnectionState.S.CLOSED);
      
      receivePacket = new TCPPacket(IS_SERVER, ip, null, theirPort, ip.getLocalAddress(), ourPort);
      sendPacket = new TCPPacket(IS_SERVER, ip, ip.getLocalAddress(), ourPort, null, theirPort);
      
      ourSeq = 0;
      theirSeq = 0;
      
      lastReceivedBuffer = null;
    }
    
    /**
     * Connects to another host. Performs the client side of the three-way handshake. If we tried
     * and failed to connect 10 times, or if the connection was closed while connecting, return
     * false. Else the connection is established and return true.
     * 
     * @param dst The IP address of the host that we want to connect to
     * @param port The port of the host that we want to connect to
     * @return Whether we are connected
     */
    public synchronized boolean connect(IpAddress dst, int port) {
      if(state.get() != ConnectionState.S.CLOSED) {
        System.err.println("Another connection in progress");
        return false;
      }
      
      state.set(ConnectionState.S.SYN_SENT);
      
      // try 10 times to connect, then give up and return false
      int connectionTries;
      for(connectionTries = 0; connectionTries < 10 && state.get() == ConnectionState.S.SYN_SENT; connectionTries++) {
        // send SYN packet
        
        if(System.getProperty("debug.seq") != null) {
          ourSeq = Long.MAX_VALUE;
        }
        else {
          ourSeq = 0;
        }
        
        theirSeq = 0;
        
        sendPacket.setDstAddress(dst);
        sendPacket.setDstPort((short)port);
        
        try {
          sendPacket.send(ourSeq, theirSeq, FLAGS.SEND_SYN, null);
        }
        catch(IOException e) {
          System.err.println("Failed to send IP packet with SYN: " + e.getMessage());
          continue;
        }
        
        // wait for SYN-ACK packet until timeout
        
        try {
          receivePacket.receive(1);
          
          if(!(receivePacket.hasSyn() && receivePacket.hasAck())) {
            throw new InvalidPacketException("Expected SYN-ACK flags, got: " + receivePacket.getFlagsString());
          }
          
          if(receivePacket.getAckNumber() != ourSeq + 1) {
            throw new InvalidPacketException("unexpected ACK number");
          }
          
          theirSeq = receivePacket.getSeqNumber();
        }
        catch(IOException e) {
          System.err.println("Failed to receive IP packet with SYN-ACK: " + e.getMessage());
          continue;
        }
        catch(InterruptedException e) {
          System.err.println("Timeout while waiting for SYN-ACK packet: " + e.getMessage());
          continue;
        }
        catch(InvalidPacketException e) {
          System.err.println("Invalid packet received: " + e.getMessage());
          continue;
        }
        
        // send ACK back
        
        try {
          sendPacket.send(ourSeq + 1, theirSeq + 1, FLAGS.SEND_ACK, null);
        }
        catch(IOException e) {
          System.err.println("Failed to send IP packet with ACK");
          continue;
        }
        
        /*
         * If we reach this point, the ACK has been successfully sent and the connection is
         * established
         */
        
        ourSeq++;
        theirSeq++;
        state.set(ConnectionState.S.ESTABLISHED);
        return true;
      }
      
      // we tried and failed to connect 10 times or connection was closed while connecting
      if(connectionTries == 10) {
        System.err.println("Tried to connect 10 times, giving up.");
      }
      else {
        System.out.println("Connection was closed while connecting.");
      }
      state.set(ConnectionState.S.CLOSED);
      return false;
    }
    
    /**
     * Accepts a connection on this socket. This performs the server-side of the three-way
     * handshake. If a handshake fails, it continues listening: it blocks until a connection is
     * made.
     */
    public synchronized void accept() {
      if(!IS_SERVER) {
        throw new Error("Client socket will never accept a connection!");
      }
      
      // wait until ready for new connection
      state.blockUntil(ConnectionState.S.CLOSED);
      
      state.set(ConnectionState.S.LISTEN);
      
      // try establishing a connection
      while(state.get() != ConnectionState.S.ESTABLISHED) {
        // try receiving SYN packet
        while(state.get() != ConnectionState.S.SYN_RECEIVED) {
          
          if(System.getProperty("debug.seq") != null) {
            ourSeq = Long.MAX_VALUE;
          }
          else {
            ourSeq = 0;
          }
          
          theirSeq = 0;
          
          try {
            receivePacket.receive(); // wait indefinitely
            
            // validate SYN packet
            if(!receivePacket.hasSyn()) {
              throw new InvalidPacketException("Expected SYN flag, got: " + receivePacket.getFlagsString());
            }
            
          }
          catch(IOException e) {
            System.err.println("Failed to receive IP packet with SYN: " + e.getMessage());
            continue;
          }
          catch(InvalidPacketException e) {
            System.err.println("Invalid packet received: " + e.getMessage());
            e.printStackTrace();
          }
          
          sendPacket.setDstAddress(receivePacket.getSrcAddress());
          sendPacket.setDstPort(receivePacket.getSrcPort());
          this.theirSeq = receivePacket.getSeqNumber();
          state.set(ConnectionState.S.SYN_RECEIVED);
        }
        
        // send SYN-ACK packet
        try {
          sendPacket.send(this.ourSeq, this.theirSeq + 1, FLAGS.SEND_SYN_ACK, null);
        }
        catch(IOException e) {
          // abort this connection and resume listening
          System.err.println("Failed to send IP packet with SYN-ACK: " + e.getMessage());
          state.set(ConnectionState.S.LISTEN);
          continue;
        }
        
        /*
         * The other side has hopefully received the SYN-ACK and thus established the connection.
         * Try 10 times to receive ACK packet and establish connection on this side as well. Beyond
         * 10 times, give up and go back to listening
         */
        
        for(int i = 0; i < 10; i++) {
          try {
            receivePacket.receive(1);
            
            if(!receivePacket.hasAck()) {
              throw new InvalidPacketException("expected: A, got: " + receivePacket.getFlagsString());
            }
            
            if(receivePacket.getSeqNumber() != theirSeq + 1) {
              throw new InvalidPacketException("unexpected SEQ number");
            }
            
            if(receivePacket.getAckNumber() != ourSeq + 1) {
              throw new InvalidPacketException("Unexpected ACK number");
            }
            
            // if we reach this point, the ACK has been received and the connection is established
            this.ourSeq += 1;
            this.theirSeq += 1;
            state.set(ConnectionState.S.ESTABLISHED);
            return;
          }
          catch(IOException e) {
            // abort this connection and resume listening
            System.err.println("Failed to receive IP packet with ACK: " + e.getMessage());
          }
          catch(InvalidPacketException e) {
            // abort this connection and resume listening
            System.err.println("Received invalid packet: " + e.getMessage());
          }
          catch(InterruptedException e) {
            // abort this connection and resume listening
            System.err.println("Failed to receive IP packet with ACK: " + e.getMessage());
          }
        }
        
        // getting the ACK failed 10 times, so we go back to listening
        state.set(ConnectionState.S.LISTEN);
      }
    }
    
    /**
     * Reads bytes from the socket into the buffer. This call is not required to return maxlen bytes
     * every time it returns. It blocks until maxlen bytes are read or the other side sent a FIN.
     * 
     * @param buf the buffer to read into
     * @param offset the offset to begin reading data into
     * @param maxlen the maximum number of bytes to read
     * @return the number of bytes read, or -1 if an error occurs.
     */
    public synchronized int read(final byte[] buf, final int offset, final int maxlen) {
      if(state.get() != ConnectionState.S.ESTABLISHED && state.get() != ConnectionState.S.CLOSE_WAIT
          && state.get() != ConnectionState.S.FIN_WAIT_1 && state.get() != ConnectionState.S.FIN_WAIT_2) {
        System.err.println("Could not read while in state " + state.get());
        return -1;
      }
      
      if(offset < 0 || offset >= buf.length) {
        System.err.println("Could not read: invalid offset (offset=" + offset + ", buf.length=" + buf.length + ")");
        return -1;
      }
      
      ByteBuffer readBuffer = ByteBuffer.wrap(buf, offset, Math.min(buf.length - offset, maxlen));
      
      // fill the read buffer with data until it is full
      
      while(readBuffer.hasRemaining()) {
        
        // if our last received packet still has some data left, first put that data into our read
        // buffer
        
        if(lastReceivedBuffer != null && lastReceivedBuffer.hasRemaining()) {
          int remaining = lastReceivedBuffer.remaining();
          int bytesToWrite = Math.min(remaining, readBuffer.remaining());
          System.out.printf("Received packet has %d B remaining, put %d B in read buffer%n", remaining, bytesToWrite);
          int packetOffset = lastReceivedBuffer.arrayOffset() + lastReceivedBuffer.position();
          readBuffer.put(lastReceivedBuffer.array(), packetOffset, bytesToWrite);
          lastReceivedBuffer.position(lastReceivedBuffer.position() + bytesToWrite);
          continue;
        }
        
        // if we received a FIN last time, we go into passive close and return from read()
        if(receivePacket.hasFin()) {
          state.set(ConnectionState.S.CLOSE_WAIT);
          break;
        }
        
        // get new packet
        
        try {
          receivePacket.receive();
          
          if(!receivePacket.hasAck()) {
            throw new InvalidPacketException("Expected ACK packet, got: " + receivePacket.getFlagsString());
          }
          
          if(receivePacket.getSeqNumber() != theirSeq) {
            throw new InvalidPacketException("Expected SEQ=" + theirSeq + ", got: " + receivePacket.getSeqNumber());
          }
        }
        catch(IOException e) {
          System.err.println("read: Failed to receive IP packet: " + e.getMessage());
          continue;
        }
        catch(InvalidPacketException e) {
          System.err.println("read: Received invalid TCP packet: " + e.getMessage());
          System.err.println("caused by packet: " + receivePacket);
          continue;
        }
        
        if(receivePacket.getPayload().limit() > 0) {
          // beyond this point, we accepted the new packet, so we can add the payload length to
          // theirSeq
          lastReceivedBuffer = receivePacket.getPayload();
          theirSeq += lastReceivedBuffer.limit();
        }
        
        // send an acknowledgment back
        try {
          sendPacket.send(ourSeq, this.theirSeq, FLAGS.SEND_ACK, null);
        }
        catch(IOException e) {
          System.err.println("read: Failed to send IP packet with ACK: " + e.getMessage());
        }
      }
      
      return readBuffer.position() - offset;
    }
    
    /**
     * Writes to the socket from the buffer. Non-blocking.
     * 
     * @param buf the buffer to
     * @param offset the offset to begin writing data from
     * @param maxlen the number of bytes to write
     * @return the number of bytes written or -1 if an error occurs.
     */
    public synchronized int write(final byte[] buf, final int offset, final int maxlen) {
      int ackedBytes = 0;
      
      if(state.get() != ConnectionState.S.ESTABLISHED && state.get() != ConnectionState.S.CLOSE_WAIT) {
        System.err.println("Could not write while in state " + state.get());
        return -1;
      }
      
      if(offset < 0 && offset >= buf.length) {
        System.err.println("Could not write: invalid offset (offset=" + offset + ", buf.length=" + buf.length + ")");
        return -1;
      }
      
      int numErrors = 0;
      
      ByteBuffer writeBuffer = ByteBuffer.wrap(buf, offset, Math.min(buf.length - offset, maxlen));
      System.out.println("writing " + writeBuffer.remaining() + " bytes");
      
      // prepare sub-buffer with appropriate position and limit for sending first packet
      ByteBuffer subBuffer = writeBuffer.duplicate();
      subBuffer.limit(Math.min(writeBuffer.limit(), subBuffer.position() + TCPPacket.TCP_MAX_PACKET_SIZE));
      
      while(writeBuffer.hasRemaining()) {
        if(numErrors >= 10) {
          System.err.println("write: Too many errors, giving up.");
          break;
        }
        
        // send packet
        
        try {
          sendPacket.send(ourSeq, theirSeq, FLAGS.SEND_ACK, subBuffer);
        }
        catch(IOException e) {
          numErrors++;
          System.err.println("write: Couldn't send IP packet: " + e.getMessage());
          continue;
        }
        
        // wait for acknowledgment
        try {
          receivePacket.receive(1);
          
          if(!receivePacket.hasAck()) {
            throw new InvalidPacketException("Expected ACK packet, got: " + receivePacket.getFlagsString());
          }
          
          if(receivePacket.getAckNumber() != ourSeq + sendPacket.getPayload().limit()) {
            throw new InvalidPacketException("Unexpected ACK number (expected="
                + (ourSeq + sendPacket.getPayload().limit()) + ", actual=" + receivePacket.getAckNumber());
          }
        }
        catch(IOException e) {
          numErrors++;
          System.err.println("write: Failed to receive IP packet: " + e.getMessage());
          continue;
        }
        catch(InterruptedException e) {
          numErrors++;
          System.err.println("write: Timeout while waiting for an ACK packet: " + e.getMessage());
          continue;
        }
        catch(InvalidPacketException e) {
          numErrors++;
          System.err.println("write: Received invalid packet: " + e.getMessage());
          continue;
        }
        
        // beyond this point, we have sent the packet and it has been acked, so we can prepare for
        // the next packet
        ackedBytes += sendPacket.getPayload().limit();
        numErrors = 0;
        ourSeq += sendPacket.getPayload().limit();
        subBuffer.position(subBuffer.limit());
        writeBuffer.position(subBuffer.limit());
        subBuffer.limit(Math.min(writeBuffer.limit(), subBuffer.position() + TCPPacket.TCP_MAX_PACKET_SIZE));
      }
      
      // return number of bytes that were sent and acked
      return ackedBytes;
    }
    
    /**
     * Closes the connection for this socket. Non-blocking.
     * 
     * @return true unless no connection was open.
     */
    public boolean close() {
      switch(state.get()) {
        case CLOSED:
        case LISTEN:
        case SYN_SENT:
          state.set(ConnectionState.S.CLOSED);
          return false;
        case SYN_RECEIVED:
        case ESTABLISHED:
          try {
            sendPacket.send(ourSeq, theirSeq, FLAGS.SEND_FIN_ACK, null);
          }
          catch(IOException e) {
            e.printStackTrace();
          }
          state.set(ConnectionState.S.FIN_WAIT_1);
          return true;
        case CLOSE_WAIT:
          try {
            sendPacket.send(ourSeq, theirSeq, FLAGS.SEND_FIN_ACK, null);
          }
          catch(IOException e) {
            e.printStackTrace();
          }
          state.set(ConnectionState.S.LAST_ACK);
          return true;
        default:
          return true;
      }
    }
  }
  
  /**
   * Constructs a TCP stack for the given virtual address. The virtual address for this TCP stack is
   * then 192.168.1.address.
   * 
   * @param address The last octet of the virtual IP address 1-254.
   * @throws IOException if the IP stack fails to initialize.
   */
  public TCP(int address) throws IOException {
    this.ip = new IP(address);
  }
  
  /**
   * @return a new socket for this stack
   */
  public Socket socket() {
    return new Socket();
  }
  
  /**
   * @return a new server socket for this stack bound to the given port
   * @param port the port to bind the socket to.
   */
  public Socket socket(int port) {
    return new Socket(port);
  }
  
}
