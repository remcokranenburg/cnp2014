package nl.vu.cs.cn.tcp;

/**
 * ConnectionState is used as the general state of a socket. Each state represents a mode in which
 * the program is waiting for something to happen. The states are described below.
 * 
 * @author Remco Kranenburg
 */

public class ConnectionState {
  /** All states that a socket can be in. */
  public static enum S {
    LISTEN, SYN_SENT, SYN_RECEIVED, ESTABLISHED, FIN_WAIT_1, FIN_WAIT_2, CLOSE_WAIT, CLOSING, LAST_ACK, TIME_WAIT, CLOSED;
  }
  
  /** The state of the socket */
  private S s;
  
  /** Constructs the state */
  public ConnectionState(S s) {
    set(s);
  }
  
  /**
   * Blocks until the state is desired. Useful for preventing two connections at the same time.
   * 
   * @param desired Desired state.
   */
  
  public synchronized void blockUntil(ConnectionState.S desired) {
    while(s != desired) {
      try {
        this.wait();
      }
      catch(InterruptedException e) {}
    }
  }
  
  /**
   * Sets the state to another state and notifies any thread blocking on a state change.
   * 
   * @param s State to change to.
   */
  public synchronized void set(ConnectionState.S s) {
    System.out.println("state change: " + this.s + " to " + s);
    this.s = s;
    this.notifyAll();
  }
  
  /**
   * Gets the current state
   * 
   * @return The current state
   */
  public S get() {
    return this.s;
  }
}
