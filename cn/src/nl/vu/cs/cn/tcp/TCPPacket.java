package nl.vu.cs.cn.tcp;

import java.io.IOException;
import java.nio.ByteBuffer;

import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.IPUtil;

/**
 * Wraps a TCP packet. Can set and unset flags on packets, put a payload in, and send or receive it
 * over the special single-application IP layer.
 * 
 * @author Remco Kranenburg
 */

public class TCPPacket {
  public static final int   TCP_HEADER_LENGTH   = 20;
  public static final short TCP_MAX_PACKET_SIZE = 7960; // 8KB minus 40B for TCP and IP headers
                                                        
  // @formatter:off
  public static abstract class OFFSET {
    public static final int SRC_PORT    = 0,
                            DST_PORT    = 2,
                            SEQ_NUMBER  = 4,
                            ACK_NUMBER  = 8,
                            DATA_OFFSET = 12,
                            FLAGS_EXT   = 12,
                            FLAGS       = 13,
                            WINDOW_SIZE = 14,
                            CHECKSUM    = 16,
                            URG_POINTER = 18;
                            // no support for options
  }
  
  public static abstract class FLAGS {
    public static final byte FIN = 0x01, // 0b00000001
                             SYN = 0x02, // 0b00000010 
                             RST = 0x04, // 0b00000100
                             PSH = 0x08, // 0b00001000
                             ACK = 0x10, // 0b00010000
                             SEND_SYN = SYN | PSH,
                             SEND_SYN_ACK = SYN | ACK | PSH,
                             SEND_ACK = ACK | PSH,
                             SEND_FIN_ACK = FIN | ACK | PSH;
  }  
  // @formatter:on
  
  private final boolean   IS_SERVER;
  private final IP        ip;
  private final IP.Packet ipPacket;
  
  private IpAddress       srcAddress;
  private short           srcPort;
  private IpAddress       dstAddress;
  private short           dstPort;
  private ByteBuffer      ipPayload;
  private ByteBuffer      tcpPayload;
  
  /**
   * Constructs a TCP packet. Sets the connection information so it can be correctly sent. The
   * source address can be set to null and the port to -1 if it is unknown (this happens in
   * Socket.accept when it waits for a SYN packet to arrive).
   * 
   * @param isServer Used to correctly log the packet's trajectory when it is sent or received.
   * @param ip The IP subsystem to use for sending or receiving.
   * @param srcAddress Source address of the packet
   * @param srcPort Source port of the packet
   * @param dstAddress Destination address of the packet
   * @param dstPort Destination port of the packet
   */
  
  public TCPPacket(boolean isServer, IP ip, IpAddress srcAddress, short srcPort, IpAddress dstAddress, short dstPort) {
    this.IS_SERVER = isServer;
    this.ip = ip;
    this.ipPacket = new IP.Packet();
    this.ipPayload = IPUtil.getIpDataBuffer(ipPacket);
    this.ensureIpPayloadCapacity(TCP_HEADER_LENGTH);
    this.tcpPayload = this.getPayload();
    this.srcAddress = srcAddress;
    this.srcPort = srcPort;
    this.dstAddress = dstAddress;
    this.dstPort = dstPort;
    
    if(srcAddress != null) {
      IPUtil.setSrcAddress(ipPacket, srcAddress);
    }
    
    if(dstAddress != null) {
      IPUtil.setDstAddress(ipPacket, dstAddress);
    }
  }
  
  /**
   * Receives a packet from the IP layer. Decodes it and logs it. Waits indefinitely until a packet
   * is received.
   * 
   * @throws IOException If receiving the packet failed.
   * @throws InvalidPacketException If the received packet was determined to be invalid. Some states
   *           are invalid depending on the context of the packet, which can not be examined here.
   *           Such situations are accounted for inside the five Socket methods.
   */
  
  public void receive() throws IOException, InvalidPacketException {
    ip.ip_receive(ipPacket);
    this.decode();
    System.out.println(log(false));
  }
  
  /**
   * Receives a packet from the IP layer. Decodes it and logs it. Waits a maximum of timeout
   * seconds.
   * 
   * @param timeout The time to wait for a packet to arrive.
   * @throws IOException If receiving the packet failed.
   * @throws InterruptedException If no packet arrived within the timeout
   * @throws InvalidPacketException If the received packet was determined to be invalid. Some states
   *           are invalid depending on the context of the packet, which can not be examined here.
   *           Such situations are accounted for inside the five Socket methods.
   */
  
  public void receive(int timeout) throws IOException, InterruptedException, InvalidPacketException {
    ip.ip_receive_timeout(ipPacket, timeout);
    this.decode();
    System.out.println(this.log(false));
  }
  
  /**
   * Sends a packet through the IP layer. Encodes it, logs it, then sends it.
   * 
   * @param seqNumber Sequence number to encode in the packet before sending.
   * @param ackNumber Acknowledgment number to encode in the packet before sending.
   * @param flags Flags to use for sending a packet. Use the SEND_* flags in FLAGS to send packets
   *          with valid flags.
   * @param tcpPayload The payload to send. Can be null or 0-length in case of control packets.
   * @throws IOException If sending the packet failed.
   */
  
  public void send(long seqNumber, long ackNumber, byte flags, ByteBuffer tcpPayload) throws IOException {
    this.encode(seqNumber, ackNumber, flags, tcpPayload);
    System.out.println(this.log(true));
    ip.ip_send(ipPacket);
  }
  
  /**
   * Decodes TCP packet after it has been received by the IP layer. It verifies the packet for a
   * correct checksum, a valid flag configuration, and the expected addresses and ports. Then it
   * resets the buffer positions to 0 and returns.
   * 
   * @throws InvalidPacketException If any of the validations fail.
   */
  
  public void decode() throws InvalidPacketException {
    this.ipPayload = IPUtil.getIpDataBuffer(this.ipPacket);
    final IpAddress srcAddress = IPUtil.getSrcAddress(this.ipPacket);
    final IpAddress dstAddress = IPUtil.getDstAddress(this.ipPacket);
    
    /*
     * Verify whether this packet has a valid checksum and throw InvalidPacketException if invalid
     */
    
    short receivedChecksum = ipPayload.getShort(OFFSET.CHECKSUM);
    short computedChecksum = Checksum.compute(srcAddress, dstAddress, ipPayload);
    
    if(receivedChecksum != computedChecksum) {
      throw new InvalidPacketException("checksum was wrong: received=" + receivedChecksum + " computed="
          + computedChecksum);
    }
    
    /*
     * Verify whether this packet has a valid flag configuration. For the purposes of this
     * assignment, valid flag configurations are: SYN, SYN+ACK, ACK and FIN+ACK. On incoming
     * packets, other flags are ignored. On outgoing packets, PSH should always be set and RST
     * unset; this is checked in Dispatcher.sendPacket.
     */
    
    if(!this.hasSyn() && !this.hasAck()) {
      throw new InvalidPacketException("ACK flag should be set on each non-SYN packet");
    }
    
    if(this.hasSyn() && this.hasFin()) {
      throw new InvalidPacketException("SYN and FIN both set");
    }
    
    /*
     * Verify whether this packet has the expected source and destination addresses and ports
     */
    
    if(this.srcAddress != null && this.srcAddress.getAddress() != srcAddress.getAddress()) {
      throw new InvalidPacketException("Src address expected: " + this.srcAddress + ", got: " + srcAddress);
    }
    
    if(this.dstAddress != null && this.dstAddress.getAddress() != dstAddress.getAddress()) {
      throw new InvalidPacketException("Dst address expected: " + this.dstAddress + ", got: " + dstAddress);
    }
    
    if(this.srcPort != -1 && this.srcPort != this.getSrcPort()) {
      throw new InvalidPacketException("Src port expected: " + srcPort + ", got: " + this.getSrcPort());
    }
    
    if(this.dstPort != -1 && this.dstPort != this.getDstPort()) {
      throw new InvalidPacketException("Dst port expected: " + dstPort + ", got: " + this.getDstPort());
    }
    
    /*
     * Reset buffer positions after receiving new data
     */
    
    this.ipPayload.position(TCP_HEADER_LENGTH);
    this.tcpPayload = this.ipPayload.slice();
  }
  
  /**
   * Encodes a packet for sending. May reallocate the IP payload buffer if it doesn't exist or is
   * too small. Then fills it with the header and TCP payload. It calculates the checksum and sets
   * the correct lengths for the buffers.
   * 
   * @param seqNumber The sequence number to encode in the packet.
   * @param ackNumber The acknowledgment number to encode in the packet.
   * @param flags The flags to encode in the packet. Use the SEND_* flags in FLAGS to send packets
   *          with valid flags.
   * @param buffer The TCP payload buffer to encode into the IP packet.
   */
  
  public void encode(long seqNumber, long ackNumber, byte flags, ByteBuffer buffer) {
    int tcpPayloadLength = (buffer != null ? buffer.remaining() : 0);
    
    this.ipPayload = IPUtil.getIpDataBuffer(this.ipPacket);
    
    final IpAddress srcAddress = IPUtil.getSrcAddress(this.ipPacket);
    final IpAddress dstAddress = IPUtil.getDstAddress(this.ipPacket);
    // reallocate IP payload buffer if it cannot hold the data we want to encode
    ByteBuffer tcpPayload = null;
    if(buffer != null) {
      this.ensureIpPayloadCapacity(TCP_HEADER_LENGTH + tcpPayloadLength);
      tcpPayload = buffer.duplicate();
    }
    
    // fill TCP header (without checksum)
    this.ipPayload.putShort(OFFSET.SRC_PORT, this.srcPort);
    this.ipPayload.putShort(OFFSET.DST_PORT, this.dstPort);
    this.ipPayload.putInt(OFFSET.SEQ_NUMBER, (int)seqNumber);
    this.ipPayload.putInt(OFFSET.ACK_NUMBER, (int)ackNumber);
    this.ipPayload.putInt(OFFSET.DATA_OFFSET, TCPPacket.TCP_HEADER_LENGTH);
    this.ipPayload.put(OFFSET.FLAGS, flags);
    this.ipPayload.putShort(OFFSET.WINDOW_SIZE, TCPPacket.TCP_MAX_PACKET_SIZE);
    
    // reset position of ipPayload and tcpPayload before filling it with data
    this.ipPayload.position(TCP_HEADER_LENGTH);
    this.ipPayload.limit(TCP_HEADER_LENGTH + tcpPayloadLength);
    this.tcpPayload = this.ipPayload.slice();
    this.tcpPayload.limit(0);
    
    // put TCP payload after TCP header
    if(tcpPayload != null) {
      this.ipPayload.put(tcpPayload);
      this.tcpPayload.limit(this.ipPayload.position() - this.tcpPayload.arrayOffset());
    }
    
    // calculate checksum and put it in TCP header
    final short checksum = Checksum.compute(srcAddress, dstAddress, this.ipPayload);
    this.ipPayload.putShort(OFFSET.CHECKSUM, checksum);
    
    // set IP length
    IPUtil.setIpDataBuffer(ipPacket, this.ipPayload);
  }
  
  /**
   * Gets the IP packet.
   * 
   * @deprecated Only use for testing purposes
   * @return The IP packet.
   */
  
  @Deprecated
  public IP.Packet getIpPacket() {
    return ipPacket;
  }
  
  /**
   * Gets the source address of this packet.
   * 
   * @return Source address
   */
  public IpAddress getSrcAddress() {
    return IPUtil.getSrcAddress(this.ipPacket);
  }
  
  /**
   * Gets the destination address of this packet.
   * 
   * @return Destination address
   */
  
  public IpAddress getDstAddress() {
    return IPUtil.getDstAddress(this.ipPacket);
  }
  
  /**
   * Gets the TCP payload
   * 
   * @return The TCP payload
   */
  public ByteBuffer getPayload() {
    return this.tcpPayload;
  }
  
  /**
   * Inspects the flags field to check whether this packet has the ACK flag set.
   * 
   * @return Whether the ACK flag is set
   */
  public boolean hasAck() {
    final byte flags = this.ipPayload.get(OFFSET.FLAGS);
    return (flags & FLAGS.ACK) != 0;
  }
  
  /**
   * Inspects the flags field to check whether this packet has the FIN flag set.
   * 
   * @return Whether the FIN flag is set
   */
  public boolean hasFin() {
    final byte flags = this.ipPayload.get(OFFSET.FLAGS);
    return (flags & FLAGS.FIN) != 0;
  }
  
  /**
   * Inspects the flags field to check whether this packet has the SYN flag set.
   * 
   * @return Whether the SYN flag is set
   */
  public boolean hasSyn() {
    final byte flags = this.ipPayload.get(OFFSET.FLAGS);
    return (flags & FLAGS.SYN) != 0;
  }
  
  /**
   * Inspects the flags field to check whether this packet has the PSH flag set.
   * 
   * @return Whether the PSH flag is set
   */
  public boolean hasPsh() {
    final byte flags = this.ipPayload.get(OFFSET.FLAGS);
    return (flags & FLAGS.PSH) != 0;
  }
  
  /**
   * Inspects the flags field to check whether this packet has the RST flag set.
   * 
   * @return Whether the RST flag is set
   */
  public boolean hasRst() {
    final byte flags = this.ipPayload.get(OFFSET.FLAGS);
    return (flags & FLAGS.RST) != 0;
  }
  
  /**
   * Builds a string denoting which flags are set: S for SYN, F for FIN, A for ACK, P for PSH and R
   * for RST. If all of them are set the string is: "SFAPR"
   * 
   * @return A string denoting which flags are set
   */
  public String getFlagsString() {
    final String str = "" + (this.hasSyn() ? "S" : "") + (this.hasFin() ? "F" : "") + (this.hasAck() ? "A" : "")
        + (this.hasPsh() ? "P" : "") + (this.hasRst() ? "R" : "");
    return str;
  }
  
  /**
   * Gets the sequence number from the packet
   * 
   * @return Sequence number
   */
  public long getSeqNumber() {
    return this.ipPayload.getInt(OFFSET.SEQ_NUMBER) & 0xffffffffL; // decode unsigned integer
  }
  
  /**
   * Gets the acknowledgment number from the packet
   * 
   * @return Acknowledgment number
   */
  public long getAckNumber() {
    return this.ipPayload.getInt(OFFSET.ACK_NUMBER) & 0xffffffffL; // decode unsigned integer
  }
  
  /**
   * Returns a String describing the packet in the following format: { src: srcAddress, dst:
   * dstAddress, flg: flagString, seq: seqNumber, ack: ackNumber, len: payloadLength }
   */
  
  @Override
  public String toString() {
    final String str = "{ src: " + this.getSrcAddress() + ", dst: " + this.getDstAddress() + ", flg: "
        + this.getFlagsString() + ", seq: " + this.getSeqNumber() + ", ack: " + this.getAckNumber() + ", len: "
        + this.ipPayload.limit() + " }";
    return str;
  }
  
  /**
   * Reallocates the IP payload buffer if it doesn't exist or if it isn't large enough to hold the
   * specified IP payload.
   * 
   * @param ipPayloadLength The minimum length of the buffer
   */
  
  private void ensureIpPayloadCapacity(int ipPayloadLength) {
    if(this.ipPayload == null || this.ipPayload.capacity() < ipPayloadLength) {
      this.ipPayload = ByteBuffer.allocate(ipPayloadLength);
    }
    IPUtil.setIpDataBuffer(this.ipPacket, this.ipPayload);
  }
  
  /**
   * Sets the source address field in the IP packet
   * 
   * @param address The address that will be assigned to the source address field in the IP packet
   */
  public void setSrcAddress(IpAddress address) {
    this.srcAddress = address;
    IPUtil.setSrcAddress(this.ipPacket, address);
  }
  
  /**
   * Sets the destination address field in the IP packet
   * 
   * @param address The address that will be assigned to the destination address field in the IP
   *          packet
   */
  public void setDstAddress(IpAddress address) {
    this.dstAddress = address;
    IPUtil.setDstAddress(this.ipPacket, address);
  }
  
  /**
   * Sets the source port in the corresponding field of the IP payload
   * 
   * @param port The port to set the source port field to
   */
  public void setSrcPort(short port) {
    this.srcPort = port;
    this.ipPayload.putShort(OFFSET.SRC_PORT, port);
  }
  
  /**
   * Sets the destination port in the corresponding field of the IP payload
   * 
   * @param port The port to set the destination port field to
   */
  public void setDstPort(short port) {
    this.dstPort = port;
    this.ipPayload.putShort(OFFSET.DST_PORT, port);
  }
  
  /**
   * Gets the source port from the IP payload
   * 
   * @return Source port
   */
  public short getSrcPort() {
    return this.ipPayload.getShort(OFFSET.SRC_PORT);
  }
  
  /**
   * Gets the destination port from the IP payload
   * 
   * @return Destination port
   */
  public short getDstPort() {
    return this.ipPayload.getShort(OFFSET.DST_PORT);
  }
  
  /**
   * Creates a log string based on the source and destination of the packet, and its contents. It
   * has the following format:
   * 
   * <pre>
   * PACKET |  SP -->     | src: 111.111.111.111 | dst: 111.111.111.111 | seq: 123, ack: 321 | payload: 7960B
   * PACKET |     -->  SP | src: 111.111.111.111 | dst: 111.111.111.111 | seq: 123, ack: 321 | payload: 7960B
   * PACKET |     <-- SAP | src: 111.111.111.111 | dst: 111.111.111.111 | seq: 123, ack: 321 | payload: 7960B
   * PACKET | SAP <--     | src: 111.111.111.111 | dst: 111.111.111.111 | seq: 123, ack: 321 | payload: 7960B
   * </pre>
   * 
   * @param isSend
   * @return
   */
  
  public String log(boolean isSend) {
    String flags = this.getFlagsString();
    String padding = (flags.length() < 3 ? " " : "");
    String firstPart;
    if(IS_SERVER) {
      if(isSend) {
        firstPart = "PACKET |     <-- " + padding + flags + " | ";
      }
      else {
        firstPart = "PACKET |     --> " + padding + flags + " | ";
      }
    }
    else {
      if(isSend) {
        firstPart = "PACKET | " + padding + flags + " -->     | ";
      }
      else {
        firstPart = "PACKET | " + padding + flags + " <--     | ";
      }
    }
    
    String src = "src: " + IPUtil.getSrcAddress(ipPacket) + ":" + getSrcPort() + " | ";
    String dst = "dst: " + IPUtil.getDstAddress(ipPacket) + ":" + getDstPort() + " | ";
    
    int payloadSize = ipPayload.limit() - TCP_HEADER_LENGTH;
    String seqAndAck = "seq: " + this.getSeqNumber() + ", ack: " + this.getAckNumber();
    String payload = payloadSize > 0 ? (" | payload: " + payloadSize) + "B" : "";
    
    return firstPart + src + dst + seqAndAck + payload;
  }
}
