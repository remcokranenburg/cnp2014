package nl.vu.cs.cn.tcp;

import java.nio.ByteBuffer;

import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.IPUtil;

/**
 * Utility class that deals with checksum computation and verification.
 * 
 * @author Remco Kranenburg
 */

public abstract class Checksum {
  /**
   * The pseudo header contains some additional information of a packet that is not contained within
   * that packet: source address, destination address, payload protocol and length of packet.
   */
  public static final int PSEUDO_HEADER_LENGTH = 12;
  
  /**
   * Verifies whether an IP payload has the correct checksum. Needs source and destination addresses
   * to compute the pseudo header.
   * 
   * @param srcAddress Source address of the packet
   * @param dstAddress Destination address of packet
   * @param ipPayload IP payload buffer
   * @return Whether the checksum is correct
   */
  public static boolean verify(IpAddress srcAddress, IpAddress dstAddress, ByteBuffer ipPayload) {
    final short receivedChecksum = ipPayload.getShort(TCPPacket.OFFSET.CHECKSUM);
    final short computedChecksum = Checksum.compute(srcAddress, dstAddress, ipPayload);
    
    if(receivedChecksum != computedChecksum) {
      System.err.println("Checksums don't match: received=" + receivedChecksum + " computed=" + computedChecksum);
    }
    
    return receivedChecksum == computedChecksum;
  }
  
  /**
   * Computes the checksum of the given IP payload. Needs source and destination addresses to
   * compute the pseudo header.
   * 
   * @param srcAddress Source address of the packet
   * @param dstAddress Destination address of the packet
   * @param ipPayload IP payload buffer
   * @return The checksum
   */
  
  public static short compute(IpAddress srcAddress, IpAddress dstAddress, ByteBuffer ipPayload) {
    final ByteBuffer pseudoHeader = Checksum.createPseudoHeader(srcAddress, dstAddress, ipPayload.limit());
    final ByteBuffer ipPayloadCopy = ipPayload.duplicate();
    // temporarily set checksum field to 0 for computation
    final short receivedChecksum = ipPayloadCopy.getShort(TCPPacket.OFFSET.CHECKSUM);
    ipPayloadCopy.putShort(TCPPacket.OFFSET.CHECKSUM, (short)0);
    
    // calculate checksum
    ipPayloadCopy.position(0);
    final short computedChecksum = Checksum.onesComplementSum(pseudoHeader, ipPayloadCopy);
    
    // put back old checksum value and return computed checksum
    ipPayloadCopy.putShort(TCPPacket.OFFSET.CHECKSUM, receivedChecksum);
    return computedChecksum;
  }
  
  /**
   * Creates a pseudo header from source address, destination address and payload length. Assumes
   * the TCP protocol.
   * 
   * @param srcAddress Source address of packet
   * @param dstAddress Destination address of packet
   * @param tcpLength Payload length of packet.
   * @return A buffer containing the pseudo buffer
   */
  
  private static ByteBuffer createPseudoHeader(IpAddress srcAddress, IpAddress dstAddress, int tcpLength) {
    final ByteBuffer buffer = ByteBuffer.allocate(Checksum.PSEUDO_HEADER_LENGTH);
    
    buffer.putInt(IPUtil.swapByteOrder(srcAddress.getAddress()));
    buffer.putInt(IPUtil.swapByteOrder(dstAddress.getAddress()));
    buffer.put((byte)0); // reserved
    buffer.put((byte)IP.TCP_PROTOCOL);
    buffer.putShort((short)tcpLength);
    buffer.flip();
    
    return buffer;
  }
  
  /**
   * Computes the ones complement sum of all provided buffers. In a loop, it adds two bytes to the
   * sum, then when it creates a 16-bit carry, that carry is added to the front again. An odd number
   * of bytes is padded with a 0 byte. Finally, take the ones complement of the sum.
   * 
   * @param buffers The buffers to sum together
   * @return The ones complement sum of all provided buffers
   */
  private static short onesComplementSum(ByteBuffer... buffers) {
    int sum = 0;
    
    for(final ByteBuffer buffer : buffers) {
      while(buffer.hasRemaining()) {
        // add odd last byte with a 0x0 byte padding
        if(buffer.remaining() < 2) {
          sum += buffer.get() << 8;
        }
        else {
          sum += buffer.getChar();
        }
        
        // add the carry (if any) to the front
        sum = (sum + (sum >>> 16)) & 0xffff;
      }
      buffer.rewind();
    }
    
    return (short)(~sum & 0xffff);
  }
}
