package nl.vu.cs.cn.tcp;

/**
 * InvalidPacketException is thrown when a packet has been deemed invalid and should be rejected,
 * either during sending or receiving. In case of a receive, the packet is not ACKed and will
 * eventually be retransmitted.
 * 
 * @author Remco Kranenburg
 */

public class InvalidPacketException extends Exception {
  private static final long serialVersionUID = 1;
  
  /** Constructs the exception with a message */
  public InvalidPacketException(String msg) {
    super(msg);
  }
}
