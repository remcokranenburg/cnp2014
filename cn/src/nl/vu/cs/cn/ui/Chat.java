package nl.vu.cs.cn.ui;

import nl.vu.cs.cn.R;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

/**
 * Chat controls the root user interface and is the main entry point of the program. It consists of
 * a single view pager, which is connected here to a ChatPageAdapter.
 * 
 * @author Remco Kranenburg
 */

public class Chat extends FragmentActivity {
  private ChatPagerAdapter adapter;
  private ViewPager        pager;
  
  /**
   * Called when the activity is first created.
   */
  
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.main);
    
    this.adapter = new ChatPagerAdapter(this.getSupportFragmentManager());
    this.pager = (ViewPager)this.findViewById(R.id.pager);
    this.pager.setAdapter(this.adapter);
  }
}
