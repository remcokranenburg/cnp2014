package nl.vu.cs.cn.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * ChatPagerAdapter is responsible for adding fragments to the Chat pager. It adds a fragment for
 * each participant in the chat and gives that fragment the necessary information in a Bundle. It is
 * also responsible for providing titles for the tabs.
 * 
 * @author Remco Kranenburg
 */

public class ChatPagerAdapter extends FragmentPagerAdapter {
  String[] participants = { "Alice", "Bob" };
  
  /**
   * Constructs the adapter that creates and returns the fragments as needed by the pager in Chat.
   * 
   * @param fragmentManager Manages all fragments created by this adapter. This is what links the
   *          fragments to the pager in Chat.
   */
  
  public ChatPagerAdapter(FragmentManager fragmentManager) {
    super(fragmentManager);
    
  }
  
  /**
   * Gets the "chat window" fragment (or tab) of participant i. Loads the list of participants into
   * the fragment as a bundle.
   * 
   * @param i The participant to get the fragment for.
   */
  
  @Override
  public Fragment getItem(int i) {
    final Fragment fragment = new ChatFragment();
    final Bundle arguments = new Bundle();
    arguments.putStringArray("participants", this.participants);
    arguments.putInt("participantId", i);
    fragment.setArguments(arguments);
    return fragment;
  }
  
  /**
   * Gets the number of fragments (tabs).
   * 
   * @return The number of fragments.
   */
  
  @Override
  public int getCount() {
    return this.participants.length;
  }
  
  /**
   * Gets the title of a fragment.
   * 
   * @return Title of a fragment
   */
  
  @Override
  public CharSequence getPageTitle(int i) {
    return this.participants[i];
  }
}
