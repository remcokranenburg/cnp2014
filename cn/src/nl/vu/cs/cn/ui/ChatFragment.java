package nl.vu.cs.cn.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.R;
import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.TCP.Socket;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

/**
 * ChatFragment controls the behavior of a chat page in the tabbed interface of the Chat user
 * interface. It connects chat history list to its backing data array, handles Send button clicks,
 * and sets the correct hint in the text entry box.
 * 
 * @author Remco Kranenburg
 */

public class ChatFragment extends Fragment {
  // chat data
  private String[]          participants;
  private int               id;
  private ArrayList<String> historyData;
  private TCP               tcp;
  private Socket            socket;
  private Queue<byte[]>     messageQueue;
  
  // user interface elements
  private View              view;
  private EditText          messageText;
  private Button            sendButton;
  private ListView          chatHistory;
  private ClickHandler      clickHandler;
  
  /**
   * Handles a click of the send button. Adds message to the chatHistory and adds it to the Queue to
   * be sent to the other side.
   * 
   * @author Remco Kranenburg
   */
  private class ClickHandler implements View.OnClickListener {
    
    public ClickHandler(View view) {}
    
    @Override
    public void onClick(View view) {
      messageText.append("\n");
      String message = messageText.getText().toString();
      messageText.setText("");
      
      if(message.compareTo("\n") != 0) {
        ChatFragment.this.addMessage(ChatFragment.this.id, message);
        byte[] bytes = message.getBytes();
        messageQueue.add(bytes);
      }
    }
  }
  
  /**
   * Main entry point of this fragment. Constructs the interface and sets up the TCP connection
   * between the participants.
   * 
   * @param layout Inflates the user interface from resource file
   * @param g Widget that contains the fragment
   * @param s Saved state of the fragment
   */
  
  @Override
  public View onCreateView(LayoutInflater layout, ViewGroup g, Bundle s) {
    // construct chat data
    final Bundle arguments = this.getArguments();
    this.participants = arguments.getStringArray("participants");
    this.id = arguments.getInt("participantId");
    this.historyData = new ArrayList<String>();
    this.messageQueue = new ConcurrentLinkedQueue<byte[]>();
    
    try {
      this.tcp = new TCP(id + 1);
    }
    catch(IOException e) {
      e.printStackTrace();
    }
    
    if(id == 0) {
      this.socket = this.tcp.socket(1337);
      
      /*
       * The server thread first receives a message, then sends a message, then waits a second. It
       * is designed to run in lock-step with the client, continuously passing messages to each
       * other. When there are no messages to send, it sends a newline which is interpreted as no
       * message.
       */
      (new Thread() {
        @Override
        public void run() {
          socket.accept();
          
          while(true) {
            // receive message (ignore newline)
            String receivedMessage = readLine(socket);
            if(receivedMessage.compareTo("\n") != 0) {
              addMessage(1, receivedMessage);
            }
            else {
              System.out.println("Received empty message, ignoring");
            }
            
            // send our message (or newline if empty)
            sendMessage(socket);
            
            // wait a while to prevent spinning
            try {
              Thread.sleep(1000);
            }
            catch(InterruptedException e) {}
          }
        }
      }).start();
    }
    else {
      this.socket = this.tcp.socket();
      
      /*
       * The client thread first sends a message, then receives a message, then waits a second. It
       * is designed to run in lock-step with the server, continuously passing messages to each
       * other. When there are no messages to send, it sends a newline which is interpreted as no
       * message.
       */
      (new Thread() {
        @Override
        public void run() {
          boolean connected = false;
          
          while(!connected) {
            connected = socket.connect(IpAddress.getAddress("192.168.0.1"), 1337);
          }
          
          while(true) {
            // send our message (or newline if empty)
            sendMessage(socket);
            
            // receive message (ignore newline)
            String receivedMessage = readLine(socket);
            if(receivedMessage.compareTo("\n") != 0) {
              addMessage(0, receivedMessage);
            }
            else {
              System.out.println("Received empty message, ignoring");
            }
            
            // wait a while to prevent spinning
            try {
              Thread.sleep(1000);
            }
            catch(InterruptedException e) {}
          }
        }
      }).start();
    }
    
    // create objects from all the necessary user interface elements
    this.view = layout.inflate(R.layout.chatfragment, g, false);
    this.messageText = (EditText)this.view.findViewById(R.id.messageText);
    this.sendButton = (Button)this.view.findViewById(R.id.sendButton);
    this.chatHistory = (ListView)this.view.findViewById(R.id.chatHistory);
    this.clickHandler = new ClickHandler(this.view);
    
    // configure the user interface
    this.setMessageTextHint();
    this.setChatHistoryAdapter();
    this.sendButton.setOnClickListener(this.clickHandler);
    
    return this.view;
  }
  
  /**
   * Helper method that pops a message of the queue and sends it, or sends a newline if the queue is
   * empty.
   * 
   * @param socket Socket to use for sending the message.
   */
  private void sendMessage(Socket socket) {
    byte[] message;
    
    /* pop message from queue and send it, except if queue is empty, send newline */
    if(null != (message = ChatFragment.this.messageQueue.poll())) {
      socket.write(message, 0, message.length);
    }
    else {
      socket.write("\n".getBytes(), 0, 1);
    }
  }
  
  /**
   * Helper method that reads a message character by character, until it reaches a newline, at which
   * point it returns the message.
   * 
   * @param socket Socket to use for reading the message.
   * @return The message
   */
  private String readLine(Socket socket) {
    byte[] receivedBytes = new byte[1];
    ArrayList<Byte> collectedBytes = new ArrayList<Byte>();
    
    while(receivedBytes[0] != '\n') {
      if(1 == socket.read(receivedBytes, 0, 1)) {
        collectedBytes.add(receivedBytes[0]);
      }
    }
    
    byte[] byteArray = new byte[collectedBytes.size()];
    for(int i = 0; i < collectedBytes.size(); i++) {
      byteArray[i] = collectedBytes.get(i);
    }
    
    return new String(byteArray);
  }
  
  /**
   * Gives the messageText a helpful hint
   */
  
  private void setMessageTextHint() {
    final String hintFormat = this.getResources().getString(R.string.messageTextHint);
    this.messageText.setHint(String.format(hintFormat, this.participants[1 - this.id]));
  }
  
  /**
   * Connects the chatHistory ListView widget to its backing data array through the use of an
   * ArrayAdapter
   */
  
  private void setChatHistoryAdapter() {
    this.chatHistory.setAdapter(new ArrayAdapter<String>(this.getActivity(), R.layout.chathistorymessage,
        this.historyData));
  }
  
  /**
   * Adds message to the chat history list.
   * 
   * @param id Id of who sent the message
   * @param messageText Text widget containing message
   */
  private void addMessage(int id, String message) {
    // add message to list data set
    historyData.add(String.format("%1$s: %2$s", participants[id], message.substring(0, message.length() - 1)));
    
    this.getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        ((ArrayAdapter<?>)chatHistory.getAdapter()).notifyDataSetChanged();
        chatHistory.setSelection(historyData.size() - 1);
      }
    });
  }
}
