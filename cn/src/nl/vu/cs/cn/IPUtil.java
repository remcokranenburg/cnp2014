package nl.vu.cs.cn;

import java.nio.ByteBuffer;

/**
 * Utility class that exposes some fields of IP.Packet to other packages. Since the test suite
 * depends on the exact signatures of the IP and TCP classes, we cannot alter these. Some fields in
 * IP.Packet have package visibility, which makes it impossible to get at them inside the tcp
 * package. Additionally, we fix byte order troubles at this point, so that every value is
 * big-endian outside the IP class.
 */

public abstract class IPUtil {
  
  /**
   * Swaps the byte order of the 32bit integer
   * 
   * @param i value to be swapped
   * @return swapped value
   */
  public static int swapByteOrder(int i) {
    // @formatter:off
    return ((i << 24)   & 0xff000000)
         | ((i << 8)    & 0x00ff0000)
         | ((i >> 8)    & 0x0000ff00)
         | ((i >>> 24)  & 0x000000ff); // >>> treats i as unsigned
    // @formatter:on
    
  }
  
  /**
   * Returns IP.Packet's payload as a ByteBuffer with correct limit
   * 
   * @param ipPacket Packet to work on.
   * @return Data buffer containing IP packet's payload
   */
  
  public static ByteBuffer getIpDataBuffer(IP.Packet ipPacket) {
    if(ipPacket.data == null) return null;
    
    final ByteBuffer data = ByteBuffer.wrap(ipPacket.data);
    data.limit(ipPacket.length);
    return data;
  }
  
  /**
   * Sets IP.Packet's payload and its length
   * 
   * @param ipPacket Packet to work on.
   * @param data Assigned to ipPacket's data. Avoids a copy operation.
   */
  
  public static void setIpDataBuffer(IP.Packet ipPacket, ByteBuffer data) {
    ipPacket.data = data.array();
    ipPacket.length = data.limit();
  }
  
  /**
   * Gets source IP address that is encoded in the packet
   * 
   * @param p Packet to work on
   * @return Source IP address
   */
  public static IP.IpAddress getSrcAddress(IP.Packet p) {
    return IP.IpAddress.getAddress(p.source);
  }
  
  /**
   * Gets destination IP address that is encoded in the packet
   * 
   * @param p Packet to work on
   * @return Destination IP address
   */
  
  public static IP.IpAddress getDstAddress(IP.Packet p) {
    return IP.IpAddress.getAddress(p.destination);
  }
  
  /**
   * Sets source address in the given IP packet
   * 
   * @param p Packet to work on
   * @param srcAddress IP address to assign to source field
   */
  public static void setSrcAddress(IP.Packet p, IP.IpAddress srcAddress) {
    p.source = srcAddress.getAddress();
  }
  
  /**
   * Sets destination address in the given IP packet
   * 
   * @param p Packet to work on
   * @param dstAddress IP address to assign to destination field
   */
  public static void setDstAddress(IP.Packet p, IP.IpAddress dstAddress) {
    p.destination = dstAddress.getAddress();
  }
}
